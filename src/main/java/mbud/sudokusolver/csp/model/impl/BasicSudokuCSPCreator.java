package mbud.sudokusolver.csp.model.impl;

import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.constraint.Constraint;
import mbud.sudokusolver.csp.constraint.impl.BoxPermutationConstraint;
import mbud.sudokusolver.csp.model.SudokuCSPCreator;
import mbud.sudokusolver.util.Pair;

import java.util.LinkedList;
import java.util.List;

public class BasicSudokuCSPCreator extends SudokuCSPCreator {

    private int width;
    private int height;
    private int totalSize;

    public BasicSudokuCSPCreator() {
        this(3, 3);
    }

    public BasicSudokuCSPCreator(int width, int height) {
        this.width = width;
        this.height = height;
        totalSize = width * height;
    }

    @Override
    public void setupVariables(List<Variable<Integer>> variables) {
        List<Integer> domain = new LinkedList<>();
        for (int i = 1; i <= totalSize; i++) {
            domain.add(i);
        }
        for (int i = 1; i <= totalSize; i++) {
            for (int j = 1; j <= totalSize; j++) {
                Variable<Integer> v = new Variable<>(new LinkedList<>(domain));
                variables.add(v);
            }
        }
    }

    @Override
    public void setupConstraints(List<Variable<Integer>> variables, List<Constraint<Integer>> constraints) {
        // Row constraints
        for (int i = 1; i <= totalSize; i++) {
            constraints.add(new BoxPermutationConstraint(valuesAtRect(variables, i, 1, i, totalSize)));
        }
        // Column constraints
        for (int i = 1; i <= totalSize; i++) {
            constraints.add(new BoxPermutationConstraint(valuesAtRect(variables, 1, i, totalSize, i)));
        }
        // Inner block constraints
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                constraints.add(new BoxPermutationConstraint(valuesAtRect(variables, 1 + height * j, 1 + width * i, (j + 1) * height, (i + 1) * width)));
            }
        }
    }

    @Override
    public Variable<Integer> valueAt(List<Variable<Integer>> variables, int x, int y) {
        return variables.get(totalSize * (x - 1) + y - 1);
    }

    public Pair<Integer, Integer> coordOf(List<Variable<Integer>> variables, Variable<Integer> v) {
        int idx = variables.indexOf(v);
        if (idx == -1) {
            return null;
        }
        return new Pair<>(idx / totalSize + 1, idx % totalSize + 1);
    }

}
