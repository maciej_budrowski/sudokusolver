package mbud.sudokusolver.csp.model.impl;

import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.constraint.Constraint;
import mbud.sudokusolver.csp.constraint.impl.SumOfVariablesConstraint;
import mbud.sudokusolver.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class KillerSudokuCSPCreator extends BasicSudokuCSPCreator {

    private List<Pair<List<Pair<Integer, Integer>>, Integer>> groups = new ArrayList<>();

    public KillerSudokuCSPCreator() {
        super(3, 3);
    }

    public void addGroup(int sum, int... coord) {
        if (coord.length <= 0 || coord.length % 2 == 1 || sum < 0 || sum < coord.length || sum > (coord.length * 9)) {
            return;
        }
        List<Pair<Integer, Integer>> listOfPairs = new ArrayList<>();
        for (int i = 0; i < coord.length; i += 2) {
            listOfPairs.add(new Pair<>(coord[i], coord[i + 1]));
        }
        groups.add(new Pair<>(listOfPairs, sum));
    }

    public void setGroups(List<Pair<List<Pair<Integer, Integer>>, Integer>> groups) {
        this.groups = groups;
    }

    @Override
    public void setupConstraints(List<Variable<Integer>> variables, List<Constraint<Integer>> constraints) {
        super.setupConstraints(variables, constraints);

        Constraint<Integer> c;
        List<Variable<Integer>> constraintVars;
        for (Pair<List<Pair<Integer, Integer>>, Integer> group : groups) {
            constraintVars = new ArrayList<>();
            for (Pair<Integer, Integer> coord : group.getA()) {
                constraintVars.add(valueAt(variables, coord.getA(), coord.getB()));
            }
            c = new SumOfVariablesConstraint(constraintVars, group.getB());
            constraints.add(c);
        }
    }
}
