package mbud.sudokusolver.csp.model.impl;

import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.constraint.Constraint;
import mbud.sudokusolver.csp.constraint.impl.BoxPermutationConstraint;
import mbud.sudokusolver.csp.model.SudokuCSPCreator;
import mbud.sudokusolver.util.Pair;

import java.util.LinkedList;
import java.util.List;

public class SamuraiSudokuCSPCreator extends SudokuCSPCreator {

    @Override
    protected void setupVariables(List<Variable<Integer>> variables) {
        List<Integer> domain = new LinkedList<>();
        for (int i = 1; i <= 9; i++) {
            domain.add(i);
        }
        for (int sq = 0; sq < 41; sq++) {
            for (int x = 0; x < 3; x++) {
                for (int y = 0; y < 3; y++) {
                    Variable<Integer> v = new Variable<>(new LinkedList<>(domain));
                    variables.add(v);
                }
            }
        }
    }

    @Override
    protected void setupConstraints(List<Variable<Integer>> variables, List<Constraint<Integer>> constraints) {
        setupRowConstraints(variables, constraints);
        setupColumnConstraints(variables, constraints);
        setupInnerBlockConstraints(variables, constraints);
    }

    private void setupInnerBlockConstraints(List<Variable<Integer>> variables, List<Constraint<Integer>> constraints) {
        for (int r = 1; r < 21; r += 3) {
            for (int c = 1; c < 21; c += 3) {
                if (r > 9 && r <= 12) {
                    if (c <= 6 || c > 15) {
                        continue;
                    }
                } else {
                    if ((r <= 6 || r > 15) && c > 9 && c <= 12) {
                        continue;
                    }
                }
                constraints.add(new BoxPermutationConstraint(valuesAtRect(variables, r, c, r + 2, c + 2)));
            }
        }
    }

    private void setupColumnConstraints(List<Variable<Integer>> variables, List<Constraint<Integer>> constraints) {
        for (int c = 1; c <= 21; c++) {
            for (int r = 1; r <= 13; r += 6) {
                if (c > 9 && c <= 12) {
                    if (r != 7) {
                        continue;
                    }
                } else if (c <= 6 || c > 15) {
                    if (r == 7) {
                        continue;
                    }
                }
                constraints.add(new BoxPermutationConstraint(valuesAtRect(variables, r, c, r + 8, c)));
            }
        }
    }

    private void setupRowConstraints(List<Variable<Integer>> variables, List<Constraint<Integer>> constraints) {
        for (int r = 1; r <= 21; r++) {
            for (int c = 1; c <= 13; c += 6) {
                if (r > 9 && r <= 12) {
                    if (c != 7) {
                        continue;
                    }
                } else if (r <= 6 || r > 15) {
                    if (c == 7) {
                        continue;
                    }
                }
                constraints.add(new BoxPermutationConstraint(valuesAtRect(variables, r, c, r, c + 8)));
            }
        }
    }

    @Override
    public Variable<Integer> valueAt(List<Variable<Integer>> variables, int x, int y) {
        if (x <= 9) {
            if (y <= 9) { // Upper-left 9x9 square
                return variables.get(9 * (x - 1) + y - 1);
            } else if (y > 12) { // Upper-right 9x9 square
                return variables.get(81 + 9 * (x - 1) + y - 13);
            } else if (x > 6) { // Upper-middle 3x3 square
                return variables.get(324 + 3 * (x - 7) + y - 10);
            }
        } else if (x > 12) {
            if (y <= 9) { // Lower-left 9x9 square
                return variables.get(162 + 9 * (x - 13) + y - 1);
            } else if (y > 12) { // Lower-right 9x9 square
                return variables.get(243 + 9 * (x - 13) + y - 13);
            } else if (x <= 15) { // Lower-middle 3x3 square
                return variables.get(333 + 3 * (x - 13) + y - 10);
            }
        } else {
            if (y > 6 && y <= 15) { // Middle 3x9 row
                return variables.get(342 + 9 * (x - 10) + y - 7);
            }
        }
        return null;
    }

    @Override
    public Pair<Integer, Integer> coordOf(List<Variable<Integer>> variables, Variable<Integer> v) {
        int i = variables.indexOf(v);
        int x;
        int y;

        if (i == -1) {
            return null;
        }
        if (i < 81) { // Upper-left 9x9 square
            x = (i / 9) + 1;
            y = (i % 9) + 1;
        } else if (i < 162) { // Upper-right 9x9 square
            x = ((i - 81) / 9) + 1;
            y = ((i - 81) % 9) + 13;
        } else if (i < 243) { // Lower-left 9x9 square
            x = ((i - 162) / 9) + 13;
            y = ((i - 162) % 9) + 1;
        } else if (i < 324) { // Lower-right 9x9 square
            x = ((i - 243) / 9) + 13;
            y = ((i - 243) % 9) + 13;
        } else if (i < 333) { // Upper-middle 3x3 square
            x = ((i - 324) / 3) + 7;
            y = ((i - 324) % 3) + 10;
        } else if (i < 342) { // Lower-middle 3x3 square
            x = ((i - 333) / 3) + 13;
            y = ((i - 333) % 3) + 10;
        } else { // Middle 3x9 row
            x = ((i - 342) / 9) + 10;
            y = ((i - 342) % 9) + 7;
        }
        return new Pair<>(x, y);
    }

}
