package mbud.sudokusolver.csp.model;

import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.constraint.Constraint;
import mbud.sudokusolver.util.Pair;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class SudokuCSPCreator {
    public final void setupBoard(List<Variable<Integer>> variables, List<Constraint<Integer>> constraints, Map<Variable<Integer>, List<Constraint<Integer>>> constraintsForVars) {
        setupVariables(variables);
        setupConstraints(variables, constraints);
        setupConstraintsForVars(constraints, constraintsForVars);
    }

    protected abstract void setupVariables(List<Variable<Integer>> variables);

    protected abstract void setupConstraints(List<Variable<Integer>> variables, List<Constraint<Integer>> constraints);

    private void setupConstraintsForVars(List<Constraint<Integer>> constraints, Map<Variable<Integer>, List<Constraint<Integer>>> constraintsForVars) {
        List<Constraint<Integer>> constraintsForKey;
        for (Constraint<Integer> c : constraints) {
            for (Variable<Integer> v : c.getScope()) {
                if (!constraintsForVars.containsKey(v)) {
                    constraintsForKey = new LinkedList<>();
                    constraintsForVars.put(v, constraintsForKey);
                } else {
                    constraintsForKey = constraintsForVars.get(v);
                }

                if (!constraintsForKey.contains(c)) {
                    constraintsForKey.add(c);
                }
            }
        }
    }

    public abstract Variable<Integer> valueAt(List<Variable<Integer>> variables, int x, int y);

    public abstract Pair<Integer, Integer> coordOf(List<Variable<Integer>> variables, Variable<Integer> v);

    protected final List<Variable<Integer>> valuesAtRect(List<Variable<Integer>> variables, int x1, int y1, int x2, int y2) {
        List<Variable<Integer>> values = new LinkedList<>();
        for (int x = x1; x <= x2; x++) {
            for (int y = y1; y <= y2; y++) {
                values.add(valueAt(variables, x, y));
            }
        }
        return values;
    }
}
