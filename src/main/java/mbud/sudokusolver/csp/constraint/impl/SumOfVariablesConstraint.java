package mbud.sudokusolver.csp.constraint.impl;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.constraint.Constraint;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SumOfVariablesConstraint extends Constraint<Integer> {

    private int sum;

    public SumOfVariablesConstraint(List<Variable<Integer>> scope, int sum) {
        super(scope);
        this.sum = sum;
    }

    @Override
    public boolean isConsistent(Assignment<Integer> assignment, Variable<Integer> var, Integer val) {
        Map<Variable<Integer>, List<Integer>> domains = new HashMap<>();
        for (Variable<Integer> v : getScope()) {
            domains.put(v, assignment.getDomainForVar(v));
        }

        int minSum = 0;
        int maxSum = 0;
        int min;
        int max;
        for (Map.Entry<Variable<Integer>, List<Integer>> e : domains.entrySet()) {
            if (e.getKey() == var) {
                minSum += val;
                maxSum += val;
                continue;
            }
            min = Integer.MAX_VALUE;
            max = 0;
            for (Integer i : e.getValue()) {
                if (i < min) {
                    min = i;
                }
                if (i > max) {
                    max = i;
                }
            }
            minSum += min;
            maxSum += max;
        }

        return sum >= minSum && sum <= maxSum;
    }

    @Override
    public int getNumberOfConflictsWithVar(Assignment<Integer> assignment, Variable<Integer> var, Integer val) {
        Map<Variable<Integer>, List<Integer>> domains = new HashMap<>();
        for (Variable<Integer> v : getScope()) {
            domains.put(v, assignment.getDomainForVar(v));
        }

        int curSum = 0;
        int curValue;
        boolean canBeLower;
        boolean canBeHigher;
        for (Map.Entry<Variable<Integer>, List<Integer>> e : domains.entrySet()) {
            if (e.getKey() == var) {
                curSum += val;
                continue;
            }

            curValue = assignment.getDomainForVar(e.getKey()).get(0);
            curSum += curValue;

            canBeHigher = canBeLower = false;
            for (Integer i : e.getKey().getDomain()) {
                if (i < curValue) {
                    canBeLower = true;
                    if (canBeHigher) {
                        break;
                    }
                }
                if (i > curValue) {
                    canBeHigher = true;
                    if (canBeLower) {
                        break;
                    }
                }
            }
        }

        if (curSum != sum) {
            return Math.abs(curSum - sum);
        }

        return 0;
    }

    @Override
    public int getNumberOfConflicts(Assignment<Integer> assignment) {
        Map<Variable<Integer>, List<Integer>> domains = new HashMap<>();
        for (Variable<Integer> v : getScope()) {
            domains.put(v, assignment.getDomainForVar(v));
        }

        int curSum = 0;
        int curValue;
        boolean canBeLower;
        boolean canBeHigher;
        for (Map.Entry<Variable<Integer>, List<Integer>> e : domains.entrySet()) {
            curValue = assignment.getDomainForVar(e.getKey()).get(0);
            curSum += curValue;

            canBeHigher = canBeLower = false;
            for (Integer i : e.getKey().getDomain()) {
                if (i < curValue) {
                    canBeLower = true;
                    if (canBeHigher) {
                        break;
                    }
                }
                if (i > curValue) {
                    canBeHigher = true;
                    if (canBeLower) {
                        break;
                    }
                }
            }
        }


        if (curSum != sum) {
            return Math.abs(curSum - sum);
        }

        return 0;
    }

    @Override
    public boolean isSatisfied(Assignment<Integer> assignment) {
        Map<Variable<Integer>, List<Integer>> domains = new HashMap<>();
        for (Variable<Integer> v : getScope()) {
            domains.put(v, assignment.getDomainForVar(v));
        }

        int curSum = 0;
        for (List<Integer> l : domains.values()) {
            if (l.size() == 1) {
                curSum += l.get(0);
            } else {
                return false;
            }
        }

        return curSum == sum;

    }

}
