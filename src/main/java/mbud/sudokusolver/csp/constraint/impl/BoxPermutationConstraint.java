package mbud.sudokusolver.csp.constraint.impl;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.constraint.Constraint;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoxPermutationConstraint extends Constraint<Integer> {

    public BoxPermutationConstraint(List<Variable<Integer>> variables) {
        super(variables);
    }

    @Override
    public boolean isConsistent(Assignment<Integer> assignment, Variable<Integer> var, Integer val) {
        Map<Variable<Integer>, List<Integer>> domains = new HashMap<>();
        for (Variable<Integer> v : getScope()) {
            domains.put(v, assignment.getDomainForVar(v));
        }

        boolean[] occurenceMap = new boolean[getScope().size()];
        for (Map.Entry<Variable<Integer>, List<Integer>> e : domains.entrySet()) {
            if (e.getKey() != var && e.getValue().size() == 1 && e.getValue().get(0) == val) {
                return false;
            } else {
                for (Integer i : e.getValue()) {
                    occurenceMap[i - 1] = true;
                }
            }
        }

        for (boolean anOccurenceMap : occurenceMap) {
            if (!anOccurenceMap) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int getNumberOfConflictsWithVar(Assignment<Integer> assignment, Variable<Integer> var, Integer val) {
        int conflicts = 0;
        for (Variable<Integer> v : getScope()) {
            if (v != var && assignment.getDomainForVar(v).get(0) == val) {
                conflicts++;
            }
        }
        return conflicts;
    }

    @Override
    public int getNumberOfConflicts(Assignment<Integer> assignment) {
        int conflicts = 0;
        boolean[] occurenceMap = new boolean[getScope().size()];
        Integer val;
        for (Variable<Integer> v : getScope()) {
            val = assignment.getDomainForVar(v).get(0);
            if (!occurenceMap[val - 1]) {
                occurenceMap[val - 1] = true;
            } else {
                conflicts++;
            }
        }
        return conflicts;
    }

    @Override
    public boolean isSatisfied(Assignment<Integer> assignment) {
        Map<Variable<Integer>, List<Integer>> domains = new HashMap<>();
        for (Variable<Integer> v : getScope()) {
            domains.put(v, assignment.getDomainForVar(v));
        }

        boolean[] wasSet = new boolean[getScope().size()];
        for (List<Integer> l : domains.values()) {
            if (l.size() == 1) {
                if (wasSet[l.get(0) - 1]) {
                    return false;
                }
                wasSet[l.get(0) - 1] = true;
            } else {
                return false;
            }
        }

        return true;
    }

}
