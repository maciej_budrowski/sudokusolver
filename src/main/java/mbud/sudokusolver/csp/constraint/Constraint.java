package mbud.sudokusolver.csp.constraint;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.Variable;

import java.util.LinkedList;
import java.util.List;

public abstract class Constraint<T> {
    private List<Variable<T>> scope;

    protected Constraint() {
        scope = new LinkedList<>();
    }

    protected Constraint(List<Variable<T>> scope) {
        this.scope = scope;
    }

    public List<Variable<T>> getScope() {
        return scope;
    }

    public boolean isConsistent(Assignment<T> assignment, Variable<T> var) {
        for (T val : assignment.getDomainForVar(var)) {
            if (!isConsistent(assignment, var, val)) {
                return false;
            }
        }
        return true;
    }

    public abstract boolean isSatisfied(Assignment<T> assignment);

    public abstract boolean isConsistent(Assignment<T> assignment, Variable<T> var, T val);

    public abstract int getNumberOfConflictsWithVar(Assignment<T> assignment, Variable<T> var, T val);

    public abstract int getNumberOfConflicts(Assignment<T> assignment);
}
