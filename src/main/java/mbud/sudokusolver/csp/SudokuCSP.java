package mbud.sudokusolver.csp;

import mbud.sudokusolver.csp.constraint.Constraint;
import mbud.sudokusolver.csp.model.SudokuCSPCreator;
import mbud.sudokusolver.util.Pair;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SudokuCSP extends CSP<Integer> {
    private List<Variable<Integer>> variables = new LinkedList<>();
    private List<Variable<Integer>> changeableVariables = new LinkedList<>();
    private List<Constraint<Integer>> constraints = new LinkedList<>();
    private Map<Variable<Integer>, List<Constraint<Integer>>> constraintsForVars = new HashMap<>();
    private SudokuCSPCreator model;

    public SudokuCSP(SudokuCSPCreator model) {
        this.model = model;
        model.setupBoard(variables, constraints, constraintsForVars);
        for (Variable<Integer> v : variables) {
            if (v.getDomain().size() <= 1) {
                continue;
            }

            changeableVariables.add(v);
        }
    }

    @Override
    public Map<Variable<Integer>, List<Constraint<Integer>>> getConstraintsForVars() {
        return constraintsForVars;
    }

    @Override
    public List<Variable<Integer>> getVariables() {
        return variables;
    }

    @Override
    public List<Constraint<Integer>> getConstraints() {
        return constraints;
    }

    @Override
    public List<Variable<Integer>> getChangeableVariables() {
        return changeableVariables;
    }

    public Variable<Integer> valueAt(int x, int y) {
        return model.valueAt(variables, x, y);
    }

    public List<Variable<Integer>> valuesAtRect(int x1, int y1, int x2, int y2) {
        List<Variable<Integer>> values = new LinkedList<>();
        for (int x = x1; x <= x2; x++) {
            for (int y = y1; y <= y2; y++) {
                values.add(model.valueAt(variables, x, y));
            }
        }
        return values;
    }

    public void assignStartingValue(Variable<Integer> v, Integer value) {
        if (variables.contains(v)) {
            v.restrictTo(value);
            if (changeableVariables.contains(v)) {
                changeableVariables.remove(v);
            }
        }
    }

    public void assignStartingValue(int x, int y, Integer value) {
        Variable<Integer> variable = valueAt(x, y);
        assignStartingValue(variable, value);
    }

    public Pair<Integer, Integer> coordOfVar(Variable<Integer> v) {
        return model.coordOf(variables, v);
    }
}
