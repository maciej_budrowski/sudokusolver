package mbud.sudokusolver.csp;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Assignment<T> {
    private Map<Variable<T>, List<T>> domains;
    private Assignment<T> parent;

    public Assignment() {
        domains = new HashMap<>();
    }

    public Assignment(Assignment<T> orig) {
        this.parent = orig;
        this.domains = new HashMap<>();
    }

    public Map<Variable<T>, List<T>> getDomains() {
        return domains;
    }

    public List<T> getDomainForVar(Variable<T> var) {
        synchronized (domains) {
            List<T> d = domains.get(var);
            if (d != null) {
                return d;
            }
            if (parent != null) {
                d = parent.getDomainForVar(var);
                if (d != null) {
                    domains.put(var, d);
                    return d;
                }
            }
            return null;
        }
    }

    public void setDomains(Map<Variable<T>, List<T>> domains) {
        synchronized (domains) {
            this.domains = domains;
        }
    }

    public Assignment<T> createAssignmentWithNewValue(Variable<T> v, T value) {
        Assignment<T> newWorld = new Assignment<>(this);
        newWorld.assignValue(v, value);
        return newWorld;
    }

    public void assignValue(Variable<T> v, T value) {
        synchronized (domains) {
            List<T> newDomain = new LinkedList<>();
            newDomain.add(value);
            domains.put(v, newDomain);
        }
    }

    public void assignDomain(Variable<T> v, List<T> domain) {
        synchronized (domains) {
            domains.put(v, new LinkedList<>(domain));
        }
    }

    public boolean hasLocalEmptyDomains() {
        for (List<T> d : getDomains().values()) {
            if (d.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public void restrictDomainTo(Variable<T> var, List<T> values) {
        synchronized (domains) {
            domains.put(var, values);
        }
    }
}
