package mbud.sudokusolver.csp;

import mbud.sudokusolver.csp.constraint.Constraint;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class CSP<T> {
    public abstract List<Variable<T>> getVariables();

    public abstract List<Constraint<T>> getConstraints();

    public abstract List<Variable<T>> getChangeableVariables();

    public abstract Map<Variable<T>, List<Constraint<T>>> getConstraintsForVars();

    public Assignment<T> startingAssignment() {
        Assignment<T> assingment = new Assignment<>();
        List<Variable<T>> variables = getVariables();
        Map<Variable<T>, List<T>> domains = assingment.getDomains();
        for (Variable<T> var : variables) {
            domains.put(var, new LinkedList<>(var.getDomain()));
        }
        return assingment;
    }

    public boolean isConsistent(Assignment<T> assignment, Variable<T> var) {
        for (Constraint<T> c : getConstraintsForVars().get(var)) {
            if (!c.isConsistent(assignment, var)) {
                return false;
            }
        }
        return true;
    }

    public boolean isSatisfied(Assignment<T> assignment) {
        for (Constraint<T> c : getConstraints()) {
            if (!c.isSatisfied(assignment)) {
                return false;
            }
        }
        return true;
    }

    public List<Constraint<T>> getConstraintsForVar(Variable<T> var) {
        return getConstraintsForVars().get(var);
    }

    public int getNumberOfConflicts(Assignment<T> assignment) {
        int conflicts = 0;
        for (Constraint<T> c : getConstraints()) {
            conflicts += c.getNumberOfConflicts(assignment);
        }
        return conflicts;
    }

    public int getNumberOfConflictsWithVar(Assignment<T> assignment, Variable<T> var) {
        return getNumberOfConflictsWithVar(assignment, var, assignment.getDomainForVar(var).get(0));
    }

    public int getNumberOfConflictsWithVar(Assignment<T> assignment, Variable<T> var, T val) {
        int conflicts = 0;
        for (Constraint<T> c : getConstraintsForVar(var)) {
            conflicts += c.getNumberOfConflictsWithVar(assignment, var, val);
        }
        return conflicts;
    }
}
