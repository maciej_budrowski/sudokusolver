package mbud.sudokusolver.csp.algorithm.heuristics;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class DomainSplitHeuristic<T> {
    public enum Type {
        BASIC_SPLIT, LEAST_USED_SPLIT, MOST_USED_SPLIT
    }

    private int numberOfCuts;

    protected DomainSplitHeuristic(int numberOfCuts) {
        this.numberOfCuts = numberOfCuts;
    }

    protected int getNumberOfCuts() {
        return numberOfCuts;
    }

    public abstract List<List<T>> getDivisionForVar(CSP<T> csp, Assignment<T> assignment, Variable<T> var);

    protected static <T> List<List<T>> splitIn(List<T> domain, int cuts) {
        if (domain == null || domain.isEmpty()) {
            return null;
        }
        if (domain.size() == 1) {
            return Collections.singletonList(domain);
        }
        cuts = Math.min(domain.size(), cuts);
        List<List<T>> divisions = new ArrayList<>(cuts);
        List<T> split;
        int separationPoint;
        int lastSeparationPoint = 0;
        for (int c = 0; c < cuts; c++) {
            separationPoint = ((c + 1) * domain.size()) / cuts;
            split = domain.subList(lastSeparationPoint, separationPoint);
            lastSeparationPoint = separationPoint;
            divisions.add(split);
        }
        return divisions;
    }

    public static List<Type> getTypes() {
        return Arrays.asList(Type.BASIC_SPLIT, Type.LEAST_USED_SPLIT, Type.MOST_USED_SPLIT);
    }
}
