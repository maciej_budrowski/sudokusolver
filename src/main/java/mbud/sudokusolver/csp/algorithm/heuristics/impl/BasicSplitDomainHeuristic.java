package mbud.sudokusolver.csp.algorithm.heuristics.impl;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.heuristics.DomainSplitHeuristic;

import java.util.List;

public class BasicSplitDomainHeuristic<T> extends DomainSplitHeuristic<T> {

    public BasicSplitDomainHeuristic() {
        super(2);
    }

    public BasicSplitDomainHeuristic(int numberOfCuts) {
        super(numberOfCuts);
    }

    @Override
    public List<List<T>> getDivisionForVar(CSP<T> csp, Assignment<T> assignment, Variable<T> var) {
        return splitIn(assignment.getDomainForVar(var), getNumberOfCuts());
    }
}
