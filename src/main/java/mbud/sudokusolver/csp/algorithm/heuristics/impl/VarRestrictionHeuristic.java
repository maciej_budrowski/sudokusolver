package mbud.sudokusolver.csp.algorithm.heuristics.impl;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.heuristics.VarSelectHeuristic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class VarRestrictionHeuristic<T> extends VarSelectHeuristic<T> {

    public enum RestrictionType {
        LEAST_RESTRICTED, MOST_RESTRICTED
    }

    private RestrictionType type;

    public VarRestrictionHeuristic(RestrictionType type) {
        this.type = type;
    }

    private class RestoreValuesCommand {
        private Map<Variable<T>, Integer> oldValues;

        private RestoreValuesCommand(Map<Variable<T>, Integer> oldValues) {
            this.oldValues = oldValues;
        }

        private void restore() {
            for (Map.Entry<Variable<T>, Integer> e : oldValues.entrySet()) {
                variableToRestrictedValsMap.put(e.getKey(), e.getValue());
            }
        }
    }

    private Map<Variable<T>, Integer> variableToRestrictedValsMap = new HashMap<>();
    private Stack<RestoreValuesCommand> restoreStack = new Stack<>();

    @Override
    public Variable<T> getNextVariable(CSP<T> csp, Assignment<T> assignment) {
        Variable<T> selectedVar = null;
        int selectedCount = -1, curCount;
        for (Variable<T> var : csp.getVariables()) {
            curCount = variableToRestrictedValsMap.get(var);
            if (curCount <= 1) {
                continue;
            }
            if (selectedVar == null) {
                selectedVar = var;
                selectedCount = curCount;
            } else {
                if ((type == RestrictionType.LEAST_RESTRICTED && curCount > selectedCount) ||
                        (type == RestrictionType.MOST_RESTRICTED && curCount < selectedCount)) {
                    selectedVar = var;
                    selectedCount = curCount;
                }
            }
        }
        return selectedVar;
    }

    @Override
    public void onDomainChanged(Assignment<T> assignment, List<Variable<T>> vars) {
        Map<Variable<T>, Integer> curValues = new HashMap<>();
        for (Variable<T> var : vars) {
            curValues.put(var, variableToRestrictedValsMap.get(var));
        }
        restoreStack.add(new RestoreValuesCommand(curValues));
        for (Variable<T> var : vars) {
            variableToRestrictedValsMap.put(var, assignment.getDomainForVar(var).size());
        }
    }

    @Override
    public void onBacktracked() {
        restoreStack.pop().restore();
    }

    @Override
    public void clear() {
        variableToRestrictedValsMap.clear();
        restoreStack.clear();
    }
}
