package mbud.sudokusolver.csp.algorithm.heuristics;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.heuristics.event.BacktrackEvent;
import mbud.sudokusolver.csp.algorithm.heuristics.event.DomainChangedEvent;

import java.util.Arrays;
import java.util.List;

public abstract class VarSelectHeuristic<T> implements DomainChangedEvent<T>, BacktrackEvent {
    public enum Type {
        FIRST_VARIABLE, LEAST_RESTRICTED_VARIABLE, MOST_RESTRICTED_VARIABLE
    }

    public abstract Variable<T> getNextVariable(CSP<T> csp, Assignment<T> assignment);

    public abstract void clear();

    public static List<Type> getTypes() {
        return Arrays.asList(Type.FIRST_VARIABLE, Type.LEAST_RESTRICTED_VARIABLE, Type.MOST_RESTRICTED_VARIABLE);
    }
}
