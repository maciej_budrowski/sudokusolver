package mbud.sudokusolver.csp.algorithm.heuristics.event;

public interface BacktrackEvent {
    void onBacktracked();
}
