package mbud.sudokusolver.csp.algorithm.heuristics.impl;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.heuristics.VarSelectHeuristic;

import java.util.List;

public class FirstVarHeuristic<T> extends VarSelectHeuristic<T> {

    @Override
    public Variable<T> getNextVariable(CSP<T> csp, Assignment<T> assignment) {
        for (Variable<T> v : csp.getVariables()) {
            if (assignment.getDomainForVar(v).size() > 1) {
                return v;
            }
        }

        return null;
    }

    @Override
    public void onDomainChanged(Assignment<T> assignment, List<Variable<T>> vars) {
        //Do nothing
    }

    @Override
    public void onBacktracked() {
        //Do nothing
    }

    @Override
    public void clear() {
        //Do nothing

    }
}
