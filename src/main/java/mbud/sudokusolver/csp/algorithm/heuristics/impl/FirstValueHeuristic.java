package mbud.sudokusolver.csp.algorithm.heuristics.impl;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.heuristics.ValueSelectHeuristic;

import java.util.List;

public class FirstValueHeuristic<T> extends ValueSelectHeuristic<T> {
    @Override
    public List<T> getValuesForVariable(CSP<T> csp, Assignment<T> assignment, Variable<T> var) {
        return assignment.getDomainForVar(var);
    }
}
