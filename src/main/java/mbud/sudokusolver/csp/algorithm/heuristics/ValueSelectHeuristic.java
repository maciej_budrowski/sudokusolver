package mbud.sudokusolver.csp.algorithm.heuristics;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;

import java.util.Arrays;
import java.util.List;

public abstract class ValueSelectHeuristic<T> {
    public enum Type {
        FIRST_VALUE, LEAST_USED_VALUE, MOST_USED_VALUE
    }

    public abstract List<T> getValuesForVariable(CSP<T> csp, Assignment<T> assignment, Variable<T> var);

    public static List<Type> getTypes() {
        return Arrays.asList(Type.FIRST_VALUE, Type.LEAST_USED_VALUE, Type.MOST_USED_VALUE);
    }
}
