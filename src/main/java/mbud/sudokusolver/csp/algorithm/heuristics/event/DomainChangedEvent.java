package mbud.sudokusolver.csp.algorithm.heuristics.event;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.Variable;

import java.util.List;

public interface DomainChangedEvent<T> {
    void onDomainChanged(Assignment<T> assignment, List<Variable<T>> vars);
}
