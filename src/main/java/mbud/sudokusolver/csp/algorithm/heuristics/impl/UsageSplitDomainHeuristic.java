package mbud.sudokusolver.csp.algorithm.heuristics.impl;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.heuristics.DomainSplitHeuristic;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class UsageSplitDomainHeuristic<T> extends DomainSplitHeuristic<T> {
    public enum UsageType {
        LEAST_USED, MOST_USED
    }

    private UsageType type;

    public UsageSplitDomainHeuristic(UsageType type, int numberOfCuts) {
        super(numberOfCuts);
        this.type = type;
    }

    @Override
    public List<List<T>> getDivisionForVar(CSP<T> csp, Assignment<T> assignment, Variable<T> var) {
        ArrayList<T> domain = new ArrayList<>(assignment.getDomainForVar(var));
        int[] counts = new int[domain.size()];

        List<T> innerDomain;
        int idx;
        for (Variable<T> variable : csp.getVariables()) {
            innerDomain = assignment.getDomainForVar(variable);
            if (innerDomain.size() != 1) {
                continue;
            }
            idx = domain.indexOf(innerDomain.get(0));
            if (idx == -1) {
                continue;
            }
            counts[idx]++;
        }
        Comparator<T> sorter;
        if (type == UsageType.LEAST_USED) {
            sorter = (o1, o2) -> counts[domain.indexOf(o2)] - counts[domain.indexOf(o1)];
        } else {
            sorter = (o1, o2) -> counts[domain.indexOf(o1)] - counts[domain.indexOf(o2)];
        }
        domain.sort(sorter);
        return splitIn(domain, getNumberOfCuts());
    }

}
