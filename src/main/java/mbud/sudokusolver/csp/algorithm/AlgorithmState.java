package mbud.sudokusolver.csp.algorithm;

public interface AlgorithmState {
    void step();
}
