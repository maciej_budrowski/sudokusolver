package mbud.sudokusolver.csp.algorithm;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.exception.EmptyDomainException;
import mbud.sudokusolver.csp.algorithm.heuristics.VarSelectHeuristic;
import mbud.sudokusolver.csp.algorithm.impl.DomainSplitting;
import mbud.sudokusolver.csp.algorithm.impl.GAC;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public abstract class BacktrackingAlgorithm<T> extends SolvingAlgorithm<T> {

    public class InitialGACState implements AlgorithmState {
        @Override
        public void step() {
            changedVariables = new ArrayList<>();
            try {
                gac.solve(getAssignment(), changedVariables);
            } catch (EmptyDomainException e) {
                finished = true;
                sendMessage(SolvingAlgorithm.MessageType.SOLUTION_NOT_FOUND);
                notifyAlgorithmEnded(null);
                return;
            }

            notifyDomainChanged(getAssignment(), changedVariables);

            if (getCSP().isSatisfied(getAssignment())) {
                finished = true;
                sendMessage(SolvingAlgorithm.MessageType.SOLUTION_FOUND);
                notifyAlgorithmEnded(getAssignment());
                return;
            }

            varSelectHeuristic.onDomainChanged(getAssignment(), changedVariables);
            setCurrentState(new DomainSplitting.SelectVariableState());
        }
    }

    public class SelectVariableState implements AlgorithmState {
        @Override
        public void step() {
            Variable<T> var = getNextVariable();
            sendMessage(MessageType.VARIABLE_SELECTED, var);
            if (var == null) {
                sendMessage(MessageType.BACKTRACK_OUT_OF_VARS);
                BacktrackingAlgorithm.this.doBacktrack();
                return;
            }
            setCurrentState(getBacktrackableState(var));
        }

        private Variable<T> getNextVariable() {
            return varSelectHeuristic.getNextVariable(getCSP(), getAssignment());
        }
    }

    protected abstract class BacktrackableState<U> implements AlgorithmState {
        protected List<U> collection;
        protected Iterator<U> iterator;
        protected Assignment<T> startingAssignment;
        protected List<Variable<T>> variablesWithChangedDomains;
        protected Variable<T> variable;

        protected BacktrackableState(Variable<T> variable) {
            this.variable = variable;
            startingAssignment = getAssignment();
        }

        @Override
        public void step() {
            if (iterator.hasNext()) {
                iterations++;
                List<Variable<T>> changedVariablesState = new ArrayList<>();
                Assignment<T> newAssign = assignDomainAndCheckConsistency(changedVariablesState);
                if (newAssign == null) {
                    return;
                }

                curTreeHeight++;
                notifyDomainChanged(newAssign, changedVariablesState);

                if (getCSP().isSatisfied(newAssign)) {
                    finished = true;
                    setAssignment(newAssign);
                    sendMessage(SolvingAlgorithm.MessageType.SOLUTION_FOUND);
                    notifyAlgorithmEnded(newAssign);
                    return;
                }

                varSelectHeuristic.onDomainChanged(newAssign, changedVariablesState);
                variablesWithChangedDomains = changedVariablesState;
                setAssignment(newAssign);
                stateStack.push(this);
                setCurrentState(new SelectVariableState());
            } else {
                if (!stateStack.empty()) {
                    BacktrackingAlgorithm.this.doBacktrack();
                    sendMessage(MessageType.BACKTRACK_OUT_OF_VALUES);
                    backtracks++;
                } else {
                    finished = true;
                    sendMessage(MessageType.BACKTRACK_NO_STATES);
                    backtracks++;
                    notifyAlgorithmEnded(null);
                }
            }
        }

        protected abstract Assignment<T> assignDomainAndCheckConsistency(List<Variable<T>> changedVariablesState);

        private void backtrack() {
            curTreeHeight--;
            varSelectHeuristic.onBacktracked();
            setAssignment(startingAssignment);
            notifyDomainChanged(startingAssignment, variablesWithChangedDomains);
        }
    }

    public enum MessageType {
        VARIABLE_SELECTED, BACKTRACK_OUT_OF_VARS, BACKTRACK_OUT_OF_VALUES, BACKTRACK_NO_STATES, BACKTRACK_GAC_EMPTY_DOMAIN
    }

    protected VarSelectHeuristic<T> varSelectHeuristic;
    protected GAC<T> gac;
    protected List<Variable<T>> changedVariables;

    protected boolean finished = false;

    protected long iterations = 0;
    protected long backtracks = 0;
    protected long curTreeHeight = 0;

    protected Stack<BacktrackableState> stateStack = new Stack<>();

    protected BacktrackingAlgorithm(CSP<T> csp) {
        super(csp);
    }

    protected BacktrackingAlgorithm(CSP<T> csp, GAC<T> gac) {
        super(csp);
        this.gac = gac;
    }

    protected abstract BacktrackableState getBacktrackableState(Variable<T> variable);

    protected void doBacktrack() {
        BacktrackableState state = stateStack.pop();
        state.backtrack();
        setCurrentState(state);
    }

    @Override
    public void init() {
        finished = false;
        stateStack.clear();
        iterations = backtracks = curTreeHeight = 0;
        setAssignment(getCSP().startingAssignment());
        varSelectHeuristic.clear();
        varSelectHeuristic.onDomainChanged(getAssignment(), getCSP().getVariables());
        notifyDomainChanged(getAssignment(), getCSP().getVariables());
        if (gac != null) {
            gac.clear();
        }
    }

    @Override
    public boolean hasEnded() {
        return finished;
    }

    public long getBacktracks() {
        return backtracks;
    }

    public long getIterations() {
        return iterations;
    }

    public long getGACIterations() {
        if (gac == null) {
            return 0;
        }
        return gac.getIterations();
    }
}
