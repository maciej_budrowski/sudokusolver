package mbud.sudokusolver.csp.algorithm;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.exception.EmptyDomainException;
import mbud.sudokusolver.csp.algorithm.impl.GAC;
import mbud.sudokusolver.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;

public abstract class LocalSearch<T> extends SolvingAlgorithm<T> {

    public class InitialGACState implements AlgorithmState {
        @Override
        public void step() {
            Assignment<T> ass = getCSP().startingAssignment();
            List<Variable<T>> changedVariables = new ArrayList<>();
            try {
                gac.solve(ass, changedVariables);
            } catch (EmptyDomainException e) {
                hasFinished = true;
                sendMessage(SolvingAlgorithm.MessageType.SOLUTION_NOT_FOUND);
                setAssignment(null);
                notifyAlgorithmEnded(null);
            }

            notifyDomainChanged(ass, changedVariables);

            if (getCSP().isSatisfied(ass)) {
                hasFinished = true;
                sendMessage(SolvingAlgorithm.MessageType.SOLUTION_FOUND);
                setAssignment(ass);
                notifyAlgorithmEnded(ass);
                return;
            }

            setCurrentState(new InitRunState());
        }
    }

    public class InitRunState implements AlgorithmState {
        @Override
        public void step() {
            sendMessage(MessageType.STARTED_RUN);
            List<Variable<T>> changedVariables = new ArrayList<>();
            setAssignment(generateRandom(changedVariables));
            notifyDomainChanged(getAssignment(), changedVariables);
            setCurrentState(getNormalStepState());
        }
    }

    private enum MessageType {
        STARTED_RUN
    }

    private Random random = new Random();

    protected GAC<T> gac;
    protected boolean hasFinished;

    protected LocalSearch(CSP<T> csp) {
        super(csp);
    }

    protected Assignment<T> generateRandom() {
        return generateRandom(null, null);
    }

    protected Assignment<T> generateRandom(Assignment<T> assignment) {
        return generateRandom(assignment, null);
    }

    protected Assignment<T> generateRandom(List<Variable<T>> changedVariables) {
        return generateRandom(null, changedVariables);
    }

    protected Assignment<T> generateRandom(Assignment<T> assignment, List<Variable<T>> changedVariables) {
        if (assignment == null) {
            assignment = getCSP().startingAssignment();
        }
        int i;
        List<T> domain;
        for (Variable<T> v : getCSP().getChangeableVariables()) {
            domain = v.getDomain();
            i = random.nextInt(domain.size());
            assignment.assignValue(v, domain.get(i));
        }
        if (changedVariables != null) {
            changedVariables.addAll(getCSP().getChangeableVariables());
        }
        return assignment;
    }

    protected Assignment<T> randomWalk(Assignment<T> assignment) {
        return randomWalk(assignment, null);
    }

    protected Assignment<T> randomWalk(Assignment<T> assignment, List<Variable<T>> changedVariables) {
        int numOfVars = getCSP().getChangeableVariables().size();
        //3-12 steps
        int steps = 3 + random.nextInt(10);
        int i;
        Variable<T> var;
        List<T> domain;
        while (steps > 0) {
            i = random.nextInt(numOfVars);
            var = getCSP().getChangeableVariables().get(i);
            if (changedVariables != null) {
                changedVariables.add(var);
            }
            domain = var.getDomain();
            i = random.nextInt(domain.size());
            assignment.assignValue(var, domain.get(i));
            steps--;
        }
        return assignment;
    }

    protected Pair<Variable<T>, T> getNextVariable(Assignment<T> assignment) {
        PriorityQueue<Pair<Variable<T>, T>> queue = new PriorityQueue<>((o1, o2) -> {
            int conflicts1 = 0;
            int conflicts2 = 0;
            conflicts1 -= getCSP().getNumberOfConflictsWithVar(assignment, o1.getA());
            conflicts1 += getCSP().getNumberOfConflictsWithVar(assignment, o1.getA(), o1.getB());
            conflicts2 -= getCSP().getNumberOfConflictsWithVar(assignment, o2.getA());
            conflicts2 += getCSP().getNumberOfConflictsWithVar(assignment, o2.getA(), o2.getB());
            return conflicts1 - conflicts2;
        });
        for (Variable<T> v : getCSP().getChangeableVariables()) {
            for (T val : v.getDomain()) {
                if (assignment.getDomainForVar(v).get(0) == val) {
                    continue;
                }

                queue.add(new Pair<>(v, val));
            }
        }

        return queue.poll();
    }

    protected Pair<Variable<T>, T> getNextVariableTwoStep(Assignment<T> assignment) {
        PriorityQueue<Pair<Variable<T>, T>> queue = new PriorityQueue<>((o1, o2) -> {
            int conflicts1 = 0;
            int conflicts2 = 0;
            conflicts1 -= getCSP().getNumberOfConflictsWithVar(assignment, o1.getA());
            conflicts1 += getCSP().getNumberOfConflictsWithVar(assignment, o1.getA(), o1.getB());
            conflicts2 -= getCSP().getNumberOfConflictsWithVar(assignment, o2.getA());
            conflicts2 += getCSP().getNumberOfConflictsWithVar(assignment, o2.getA(), o2.getB());
            return conflicts1 - conflicts2;
        });
        PriorityQueue<Variable<T>> varQueue = new PriorityQueue<>((o1, o2) -> {
            int conflicts1 = getCSP().getNumberOfConflictsWithVar(assignment, o1);
            int conflicts2 = getCSP().getNumberOfConflictsWithVar(assignment, o2);
            return conflicts2 - conflicts1;
        });
        varQueue.addAll(getCSP().getChangeableVariables());
        Variable<T> var = varQueue.poll();
        List<T> domain = var.getDomain();
        for (T val : domain) {
            if (assignment.getDomainForVar(var).get(0) == val) {
                continue;
            }

            queue.add(new Pair<>(var, val));
        }

        return queue.poll();
    }

    protected Pair<Variable<T>, T> getNextVariableRandomConflict(Assignment<T> assignment) {
        PriorityQueue<Pair<Variable<T>, T>> queue = new PriorityQueue<>((o1, o2) -> {
            int conflicts1 = 0;
            int conflicts2 = 0;
            conflicts1 -= getCSP().getNumberOfConflictsWithVar(assignment, o1.getA());
            conflicts1 += getCSP().getNumberOfConflictsWithVar(assignment, o1.getA(), o1.getB());
            conflicts2 -= getCSP().getNumberOfConflictsWithVar(assignment, o2.getA());
            conflicts2 += getCSP().getNumberOfConflictsWithVar(assignment, o2.getA(), o2.getB());
            return conflicts1 - conflicts2;
        });
        List<Variable<T>> conflictingVariables = new ArrayList<>();
        for (Variable<T> v : getCSP().getChangeableVariables()) {
            if (getCSP().getNumberOfConflictsWithVar(assignment, v) > 0) {
                conflictingVariables.add(v);
            }
        }
        int i = random.nextInt(conflictingVariables.size());
        Variable<T> var = conflictingVariables.get(i);
        List<T> domain = var.getDomain();
        for (T val : domain) {
            if (assignment.getDomainForVar(var).get(0) == val) {
                continue;
            }

            queue.add(new Pair<>(var, val));
        }

        return queue.poll();
    }

    protected Pair<Variable<T>, T> getRandomAssignment() {
        List<Variable<T>> changeableVariables = new ArrayList<>(getCSP().getChangeableVariables());
        int i = random.nextInt(changeableVariables.size());
        Variable<T> var = changeableVariables.get(i);
        List<T> domain = var.getDomain();
        return new Pair<>(var, domain.get(random.nextInt(domain.size())));
    }

    protected abstract AlgorithmState getNormalStepState();

}
