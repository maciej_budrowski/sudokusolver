package mbud.sudokusolver.csp.algorithm.enums;

public enum AlgorithmStatistic {
    TIME_PASSED(false, 0),
    VISITED_NODES(false, 0),
    BACKTRACKS(false, 0),
    TREE_HEIGHT(false, 0),
    GAC_ITERATIONS(false, 0),
    CUR_STEP(false, 0),
    CUR_RESTART(true, 0),
    TOTAL_RESTARTS(true, 0),
    TOTAL_STEPS(false, 0),
    CUR_CONFLICTS(false, 0),
    MIN_CONFLICTS(false, 0),
    CUR_TEMPERATURE(false, 6),
    CUR_MINIMA_VISITED(false, 0),
    SUCCESS(true, 0);

    private boolean temporal = false;
    private int precision = 2;

    AlgorithmStatistic(boolean temporal, int precision) {
        this.temporal = temporal;
        this.precision = precision;
    }

    public boolean isTemporal() {
        return temporal;
    }

    public int getPrecision() {
        return precision;
    }
}
