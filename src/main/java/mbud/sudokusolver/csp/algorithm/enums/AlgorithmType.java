package mbud.sudokusolver.csp.algorithm.enums;

public enum AlgorithmType {
    BACKTRACK,
    DOMAIN_SPLITTING,
    GREEDY_DESCENT,
    SIMULATED_ANNEALING
}
