package mbud.sudokusolver.csp.algorithm.exception;

public class EmptyDomainException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -8826732879663078272L;

    @Override
    public String getMessage() {
        return "Found variable with empty domain!";
    }

}
