package mbud.sudokusolver.csp.algorithm.event;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.Variable;

import java.util.List;

public interface AlgorithmDomainChanged<T> {
    void onDomainChanged(Assignment<T> assignment, List<Variable<T>> variables);
}
