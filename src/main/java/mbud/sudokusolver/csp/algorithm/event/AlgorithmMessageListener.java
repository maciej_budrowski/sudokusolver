package mbud.sudokusolver.csp.algorithm.event;

public interface AlgorithmMessageListener {
    void onMessageReceived(Enum<?> messageType, Object... arguments);
}
