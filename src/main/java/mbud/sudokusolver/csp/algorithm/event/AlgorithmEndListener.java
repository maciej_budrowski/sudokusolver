package mbud.sudokusolver.csp.algorithm.event;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmStatistic;

import java.util.Map;

public interface AlgorithmEndListener<T> {
    void onAlgorithmEnded(Assignment<T> finalAssignment, Map<AlgorithmStatistic, Number> statistics);
}
