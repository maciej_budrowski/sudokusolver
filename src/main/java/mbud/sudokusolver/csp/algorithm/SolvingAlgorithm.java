package mbud.sudokusolver.csp.algorithm;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmStatistic;
import mbud.sudokusolver.csp.algorithm.event.AlgorithmDomainChanged;
import mbud.sudokusolver.csp.algorithm.event.AlgorithmEndListener;
import mbud.sudokusolver.csp.algorithm.event.AlgorithmMessageListener;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class SolvingAlgorithm<T> {

    public enum MessageType {
        SOLUTION_FOUND, SOLUTION_NOT_FOUND
    }

    private CSP<T> csp;
    private Assignment<T> assignment;
    private Assignment<T> lastAssignment;
    private AlgorithmState currentState;
    private Set<AlgorithmMessageListener> listenerList = new HashSet<>();
    private Set<AlgorithmDomainChanged<T>> domainChangedListenerList = new HashSet<>();
    private Set<AlgorithmEndListener<T>> algorithmEndListenerList = new HashSet<>();
    private boolean eventsEnabled = true;

    protected SolvingAlgorithm(CSP<T> csp) {
        this.csp = csp;
    }

    public abstract void init();

    public final Assignment<T> solve() {
        init();
        while (!hasEnded()) {
            step();
        }
        return assignment;
    }

    public final void step() {
        currentState.step();
    }

    public abstract boolean hasEnded();

    public CSP<T> getCSP() {
        return csp;
    }

    public Assignment<T> getAssignment() {
        return assignment;
    }

    protected void setAssignment(Assignment<T> assignment) {
        this.assignment = assignment;
        if (assignment != null) {
            lastAssignment = assignment;
        }
    }

    public Assignment<T> getLastAssignment() {
        return lastAssignment;
    }

    protected void setCurrentState(AlgorithmState currentState) {
        this.currentState = currentState;
    }

    public final void registerMessageListener(AlgorithmMessageListener listener) {
        listenerList.add(listener);
    }

    public final void unregisterMessageListener(AlgorithmMessageListener listener) {
        listenerList.remove(listener);
    }

    public final void registerDomainChangedListener(AlgorithmDomainChanged<T> listener) {
        domainChangedListenerList.add(listener);
    }

    public final void unregisterDomainChangedListener(AlgorithmDomainChanged<T> listener) {
        domainChangedListenerList.remove(listener);
    }

    public final void registerAlgorithmEndListener(AlgorithmEndListener<T> listener) {
        algorithmEndListenerList.add(listener);
    }

    public final void unregisterAlgorithmEndListener(AlgorithmEndListener<T> listener) {
        algorithmEndListenerList.remove(listener);
    }

    protected final void sendMessage(Enum<?> messageType, Object... arguments) {
        if (!eventsEnabled) {
            return;
        }

        for (AlgorithmMessageListener listener : listenerList) {
            listener.onMessageReceived(messageType, arguments);
        }
    }

    protected final void notifyDomainChanged(Assignment<T> assignment, List<Variable<T>> variables) {
        if (!eventsEnabled) {
            return;
        }

        for (AlgorithmDomainChanged<T> listener : domainChangedListenerList) {
            listener.onDomainChanged(assignment, variables);
        }
    }

    protected final void notifyAlgorithmEnded(Assignment<T> finalAssignment) {
        for (AlgorithmEndListener<T> endListener : algorithmEndListenerList) {
            endListener.onAlgorithmEnded(finalAssignment, getStatistics());
        }
    }

    public void setEventsEnabled(boolean eventsEnabled) {
        this.eventsEnabled = eventsEnabled;
    }

    public abstract Map<AlgorithmStatistic, Number> getStatistics();

    public abstract List<AlgorithmStatistic> getAvailableStatistics();
}