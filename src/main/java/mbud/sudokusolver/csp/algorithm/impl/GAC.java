package mbud.sudokusolver.csp.algorithm.impl;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.exception.EmptyDomainException;
import mbud.sudokusolver.csp.constraint.Constraint;
import mbud.sudokusolver.util.Pair;

import java.util.*;

public class GAC<T> {
    private CSP<T> csp;
    private long iterations = 0;
    private boolean changeBaseDomains = false;
    private final Map<Variable<T>, List<T>> baseDomains = new HashMap<>();

    public GAC(CSP<T> csp) {
        this(csp, false);
    }

    public GAC(CSP<T> csp, boolean changeBaseDomains) {
        this.csp = csp;
        this.changeBaseDomains = changeBaseDomains;
    }

    public void solve(Assignment<T> assignment, List<Variable<T>> variablesChanged) throws EmptyDomainException {
        List<Pair<Variable<T>, Constraint<T>>> arcs = new LinkedList<>();
        List<Constraint<T>> constraintsForVar;
        synchronized (baseDomains) {
            for (Variable<T> v : csp.getVariables()) {
                baseDomains.put(v, new ArrayList<>(v.getDomain()));
                constraintsForVar = csp.getConstraintsForVars().get(v);
                for (Constraint<T> c : constraintsForVar) {
                    arcs.add(new Pair<>(v, c));
                }
            }
        }
        innerSolve(assignment, arcs, variablesChanged);
    }

    public void solve(Assignment<T> assignment, Variable<T> newVar, List<Variable<T>> variablesChanged) throws EmptyDomainException {
        List<Pair<Variable<T>, Constraint<T>>> arcs = new LinkedList<>();
        List<Constraint<T>> constraintsForVar = csp.getConstraintsForVars().get(newVar);
        for (Constraint<T> c : constraintsForVar) {
            for (Variable<T> v : c.getScope()) {
                if (v == newVar) {
                    continue;
                }
                arcs.add(new Pair<>(v, c));
            }
        }
        innerSolve(assignment, arcs, variablesChanged);
    }

    private void innerSolve(Assignment<T> assignment, List<Pair<Variable<T>, Constraint<T>>> arcs, List<Variable<T>> variablesChanged) throws EmptyDomainException {
        Pair<Variable<T>, Constraint<T>> arc;
        Pair<Variable<T>, Constraint<T>> newArc;
        List<T> consistentDomain;
        boolean isConsistent;
        Iterator<T> iterator;
        while (!arcs.isEmpty()) {
            arc = arcs.remove(0);
            iterations++;
            isConsistent = true;
            consistentDomain = new ArrayList<>(assignment.getDomainForVar(arc.getA()));
            synchronized (baseDomains) {
                if (!baseDomains.containsKey(arc.getA())) {
                    baseDomains.put(arc.getA(), consistentDomain);
                }
            }
            iterator = consistentDomain.iterator();
            while (iterator.hasNext()) {
                if (!arc.getB().isConsistent(assignment, arc.getA(), iterator.next())) {
                    iterator.remove();
                    isConsistent = false;
                }
            }
            if (!isConsistent) {
                if (consistentDomain.isEmpty()) {
                    throw new EmptyDomainException();
                }
                assignment.restrictDomainTo(arc.getA(), consistentDomain);
                if (changeBaseDomains) {
                    arc.getA().restrictTo(consistentDomain);
                }
                if (variablesChanged != null && !variablesChanged.contains(arc.getA())) {
                    variablesChanged.add(arc.getA());
                }
                List<Constraint<T>> constraintsForVar = csp.getConstraintsForVars().get(arc.getA());
                for (Constraint<T> c : constraintsForVar) {
                    for (Variable<T> v : c.getScope()) {
                        if (v == arc.getA()) {
                            continue;
                        }
                        newArc = new Pair<>(v, c);
                        if (arcs.contains(newArc)) {
                            continue;
                        }
                        arcs.add(newArc);
                    }
                }
            }
        }
    }

    public long getIterations() {
        return iterations;
    }

    public void clear() {
        iterations = 0;
        if (changeBaseDomains) {
            synchronized (baseDomains) {
                for (Map.Entry<Variable<T>, List<T>> entry : baseDomains.entrySet()) {
                    entry.getKey().restrictTo(entry.getValue());
                }
            }
        }
    }
}
