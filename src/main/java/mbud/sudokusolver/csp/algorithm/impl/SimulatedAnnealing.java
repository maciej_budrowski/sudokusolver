package mbud.sudokusolver.csp.algorithm.impl;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.AlgorithmState;
import mbud.sudokusolver.csp.algorithm.LocalSearch;
import mbud.sudokusolver.csp.algorithm.SolvingAlgorithm;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmOptions;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmStatistic;
import mbud.sudokusolver.util.Pair;

import java.util.*;

public class SimulatedAnnealing<T> extends LocalSearch<T> {

    private class NormalStepState implements AlgorithmState {
        @Override
        public void step() {
            Pair<Variable<T>, T> pair = getRandomAssignment();
            sendMessage(MessageType.VARIABLE_SELECTED, pair.getA(), pair.getB());

            Assignment<T> newAssignment = getAssignment().createAssignmentWithNewValue(pair.getA(), pair.getB());
            if (canBeAccepted(getAssignment(), newAssignment)) {
                setAssignment(newAssignment);
                notifyDomainChanged(newAssignment, Collections.singletonList(pair.getA()));
                curConflicts = getCSP().getNumberOfConflicts(getAssignment());
                minConflicts = Math.min(curConflicts, minConflicts);

                if (getCSP().isSatisfied(getAssignment())) {
                    hasFinished = true;
                    sendMessage(SolvingAlgorithm.MessageType.SOLUTION_FOUND);
                    notifyAlgorithmEnded(getAssignment());
                    return;
                }
            } else {
                notifyDomainChanged(newAssignment, Collections.emptyList());
            }

            curIterations++;
            curTemperature *= temperatureMultiplier;

            if (curTemperature <= EPSILON) {
                hasFinished = true;
                sendMessage(SolvingAlgorithm.MessageType.SOLUTION_NOT_FOUND);
                setAssignment(null);
                notifyAlgorithmEnded(null);
            }
        }

        private boolean canBeAccepted(Assignment<T> oldAssignment, Assignment<T> newAssignment) {
            int oldConflicts = getCSP().getNumberOfConflicts(oldAssignment);
            int newConflicts = getCSP().getNumberOfConflicts(newAssignment);
            double probability = Math.pow(Math.E, (oldConflicts - newConflicts) / curTemperature);
            boolean decision = random.nextDouble() < probability;
            sendMessage(decision ? MessageType.ACCEPT : MessageType.REJECT, 100.0 * probability, oldConflicts, newConflicts);
            return decision;
        }
    }

    public enum MessageType {
        VARIABLE_SELECTED, ACCEPT, REJECT
    }


    private static final double EPSILON = 0.00001;
    private Random random = new Random();
    private int curIterations = 0;
    private int curConflicts = Integer.MAX_VALUE;
    private int minConflicts = Integer.MAX_VALUE;
    private double startingTemperature = 1.0;
    private double temperatureMultiplier = 0.9999;
    private double curTemperature;

    public SimulatedAnnealing(CSP<T> csp) {
        this(csp, false, 1.0, 0.99);
    }

    public SimulatedAnnealing(CSP<T> csp, boolean initialGAC, double startingTemperature, double temperatureMultiplier) {
        super(csp);
        this.startingTemperature = startingTemperature;
        this.temperatureMultiplier = temperatureMultiplier;
        if (initialGAC) {
            gac = new GAC<>(csp, true);
        }
    }

    @Override
    public void init() {
        curIterations = 0;
        curTemperature = startingTemperature;
        curConflicts = minConflicts = Integer.MAX_VALUE;
        hasFinished = false;
        notifyDomainChanged(getCSP().startingAssignment(), getCSP().getVariables());
        if (gac != null) {
            gac.clear();
            setCurrentState(new InitialGACState());
        } else {
            setCurrentState(new InitRunState());
        }
    }

    @Override
    public boolean hasEnded() {
        return hasFinished;
    }

    @Override
    protected AlgorithmState getNormalStepState() {
        return new NormalStepState();
    }

    @Override
    public Map<AlgorithmStatistic, Number> getStatistics() {
        Map<AlgorithmStatistic, Number> stats = new EnumMap<>(AlgorithmStatistic.class);
        stats.put(AlgorithmStatistic.CUR_STEP, curIterations);
        if (gac != null) {
            stats.put(AlgorithmStatistic.GAC_ITERATIONS, gac.getIterations());
        } else {
            stats.put(AlgorithmStatistic.GAC_ITERATIONS, 0);
        }
        stats.put(AlgorithmStatistic.CUR_CONFLICTS, curConflicts);
        stats.put(AlgorithmStatistic.MIN_CONFLICTS, minConflicts);
        stats.put(AlgorithmStatistic.CUR_TEMPERATURE, curTemperature);
        return stats;
    }

    @Override
    public List<AlgorithmStatistic> getAvailableStatistics() {
        return Arrays.asList(AlgorithmStatistic.CUR_STEP,
                AlgorithmStatistic.GAC_ITERATIONS,
                AlgorithmStatistic.CUR_CONFLICTS,
                AlgorithmStatistic.MIN_CONFLICTS,
                AlgorithmStatistic.CUR_TEMPERATURE);
    }

    public static List<AlgorithmOptions> getAvailableOptions() {
        return Arrays.asList(
                AlgorithmOptions.INITIAL_GAC,
                AlgorithmOptions.STARTING_TEMPERATURE,
                AlgorithmOptions.TEMPERATURE_MULTIPLIER);
    }
}
