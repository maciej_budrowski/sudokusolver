package mbud.sudokusolver.csp.algorithm.impl;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.BacktrackingAlgorithm;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmOptions;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmStatistic;
import mbud.sudokusolver.csp.algorithm.exception.EmptyDomainException;
import mbud.sudokusolver.csp.algorithm.heuristics.DomainSplitHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.VarSelectHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.impl.BasicSplitDomainHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.impl.FirstVarHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.impl.UsageSplitDomainHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.impl.VarRestrictionHeuristic;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class DomainSplitting<T> extends BacktrackingAlgorithm<T> {

    private class SplitDomainState extends BacktrackableState<List<T>> {
        public SplitDomainState(Variable<T> variable) {
            super(variable);
            collection = getDivisions(variable);
            iterator = collection.iterator();
        }

        protected Assignment<T> assignDomainAndCheckConsistency(List<Variable<T>> changedVariablesState) {
            List<T> div = iterator.next();
            sendMessage(MessageType.DOMAINS_TO_CONSIDER, collection);
            sendMessage(MessageType.DOMAIN_ASSIGNED, variable, div);
            changedVariablesState.add(variable);

            Assignment<T> newAssign = new Assignment<>(getAssignment());
            newAssign.assignDomain(variable, div);
            try {
                gac.solve(newAssign, variable, changedVariablesState);
            } catch (EmptyDomainException e) {
                sendMessage(BacktrackingAlgorithm.MessageType.BACKTRACK_GAC_EMPTY_DOMAIN);
                backtracks++;
                return null;
            }

            return newAssign;
        }

        private List<List<T>> getDivisions(Variable<T> var) {
            return domainSplitHeuristic.getDivisionForVar(getCSP(), getAssignment(), var);
        }
    }

    public enum MessageType {
        DOMAIN_ASSIGNED, DOMAINS_TO_CONSIDER
    }

    private DomainSplitHeuristic<T> domainSplitHeuristic;

    public DomainSplitting(CSP<T> csp) {
        this(csp, new FirstVarHeuristic<>(), new BasicSplitDomainHeuristic<>());
    }

    public DomainSplitting(CSP<T> csp, VarSelectHeuristic<T> varSelectHeuristic, DomainSplitHeuristic<T> domainSplitHeuristic) {
        super(csp, new GAC<>(csp));
        this.varSelectHeuristic = varSelectHeuristic;
        this.domainSplitHeuristic = domainSplitHeuristic;
    }

    public DomainSplitting(CSP<T> csp, VarSelectHeuristic.Type varSelectHeuristicType, DomainSplitHeuristic.Type domainSplitHeuristicType, int numberOfSplits) {
        super(csp, new GAC<>(csp));
        switch (varSelectHeuristicType) {
            case FIRST_VARIABLE:
                this.varSelectHeuristic = new FirstVarHeuristic<>();
                break;
            case MOST_RESTRICTED_VARIABLE:
                this.varSelectHeuristic = new VarRestrictionHeuristic<>(VarRestrictionHeuristic.RestrictionType.MOST_RESTRICTED);
                break;
            case LEAST_RESTRICTED_VARIABLE:
                this.varSelectHeuristic = new VarRestrictionHeuristic<>(VarRestrictionHeuristic.RestrictionType.LEAST_RESTRICTED);
                break;
        }
        switch (domainSplitHeuristicType) {
            case BASIC_SPLIT:
                this.domainSplitHeuristic = new BasicSplitDomainHeuristic<>(numberOfSplits);
                break;
            case LEAST_USED_SPLIT:
                this.domainSplitHeuristic = new UsageSplitDomainHeuristic<>(UsageSplitDomainHeuristic.UsageType.LEAST_USED, numberOfSplits);
                break;
            case MOST_USED_SPLIT:
                this.domainSplitHeuristic = new UsageSplitDomainHeuristic<>(UsageSplitDomainHeuristic.UsageType.MOST_USED, numberOfSplits);
                break;
        }
    }

    @Override
    protected BacktrackableState getBacktrackableState(Variable<T> variable) {
        return new SplitDomainState(variable);
    }

    @Override
    public void init() {
        super.init();
        setCurrentState(new InitialGACState());
    }

    @Override
    public Map<AlgorithmStatistic, Number> getStatistics() {
        Map<AlgorithmStatistic, Number> stats = new EnumMap<>(AlgorithmStatistic.class);
        stats.put(AlgorithmStatistic.BACKTRACKS, getBacktracks());
        stats.put(AlgorithmStatistic.VISITED_NODES, getIterations());
        stats.put(AlgorithmStatistic.TREE_HEIGHT, curTreeHeight);
        stats.put(AlgorithmStatistic.GAC_ITERATIONS, getGACIterations());
        return stats;
    }

    @Override
    public List<AlgorithmStatistic> getAvailableStatistics() {
        return Arrays.asList(AlgorithmStatistic.VISITED_NODES, AlgorithmStatistic.BACKTRACKS, AlgorithmStatistic.TREE_HEIGHT, AlgorithmStatistic.GAC_ITERATIONS);
    }

    public static List<AlgorithmOptions> getAvailableOptions() {
        return Arrays.asList(
                AlgorithmOptions.VARIABLE_HEURISTIC,
                AlgorithmOptions.SPLIT_HEURISTIC,
                AlgorithmOptions.NUMBER_OF_SPLITS);
    }
}
