package mbud.sudokusolver.csp.algorithm.impl;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.AlgorithmState;
import mbud.sudokusolver.csp.algorithm.LocalSearch;
import mbud.sudokusolver.csp.algorithm.SolvingAlgorithm;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmOptions;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmStatistic;
import mbud.sudokusolver.util.Pair;

import java.util.*;

public class GreedyDescent<T> extends LocalSearch<T> {

    private class NormalStepState implements AlgorithmState {
        @Override
        public void step() {
            curSteps++;
            totalSteps++;
            Pair<Variable<T>, T> pair = getNextVariableAndValue(getAssignment());
            sendMessage(MessageType.VARIABLE_ASSIGNED, pair.getA(), pair.getB());
            if (hasEnteredLocalMinimum(pair)) {
                curMinima++;
                if (curSteps >= maxSteps) {
                    endAlgorithm();
                    return;
                }
                setCurrentState(new LocalMinimumState());
                return;
            }
            lastVar = pair.getA();
            getAssignment().assignValue(pair.getA(), pair.getB());
            curConflicts = getCSP().getNumberOfConflicts(getAssignment());
            minConflicts = Math.min(curConflicts, minConflicts);
            notifyDomainChanged(getAssignment(), Collections.singletonList(pair.getA()));
            if (getCSP().isSatisfied(getAssignment())) {
                hasFinished = true;
                sendMessage(SolvingAlgorithm.MessageType.SOLUTION_FOUND);
                notifyAlgorithmEnded(getAssignment());
                return;
            }

            if (curSteps >= maxSteps) {
                endAlgorithm();
            }
        }

        private void endAlgorithm() {
            hasFinished = true;
            sendMessage(SolvingAlgorithm.MessageType.SOLUTION_NOT_FOUND);
            setAssignment(null);
            notifyAlgorithmEnded(null);
        }

        private Pair<Variable<T>, T> getNextVariableAndValue(Assignment<T> assignment) {
            switch (selectionMode) {
                case BEST_PAIR:
                    return getNextVariable(assignment);
                case TWO_STEP:
                    return getNextVariableTwoStep(assignment);
                case ANY_CONFLICT:
                    return getNextVariableRandomConflict(assignment);
                case RANDOM:
                    return getRandomAssignment();
            }
            return null;
        }

        private boolean hasEnteredLocalMinimum(Pair<Variable<T>, T> pair) {
            switch (selectionMode) {
                case BEST_PAIR:
                case TWO_STEP:
                    if (pair == null || (lastVar != null && pair.getA() == lastVar)) {
                        return true;
                    }
                    break;
                case ANY_CONFLICT:
                case RANDOM:
                    if (pair == null) {
                        return true;
                    }
                    break;
            }
            return false;
        }
    }

    private class LocalMinimumState implements AlgorithmState {
        @Override
        public void step() {
            sendMessage(MessageType.HANDLING_LOCAL_MINIMUM);
            List<Variable<T>> changedVariables = new ArrayList<>();
            setAssignment(handleMinimum(getAssignment(), changedVariables));
            notifyDomainChanged(getAssignment(), changedVariables);

            if (getCSP().isSatisfied(getAssignment())) {
                hasFinished = true;
                sendMessage(SolvingAlgorithm.MessageType.SOLUTION_FOUND);
                notifyAlgorithmEnded(getAssignment());
                return;
            }

            lastVar = null;
            setCurrentState(new NormalStepState());
        }

        private Assignment<T> handleMinimum(Assignment<T> assignment, List<Variable<T>> changedVariables) {
            switch (localMinimumReaction) {
                case RANDOM_RESTART:
                    return generateRandom(changedVariables);
                case RANDOM_WALK:
                    return randomWalk(assignment, changedVariables);
                default:
                    return null;
            }
        }
    }

    public enum LocalMinimumReaction {
        RANDOM_WALK, RANDOM_RESTART
    }

    public enum SelectionMode {
        BEST_PAIR, TWO_STEP, ANY_CONFLICT, RANDOM
    }

    public enum MessageType {
        VARIABLE_ASSIGNED, HANDLING_LOCAL_MINIMUM
    }

    private LocalMinimumReaction localMinimumReaction;
    private SelectionMode selectionMode;
    private int curSteps = 0;
    private int totalSteps = 0;
    private int curMinima = 0;
    private int minConflicts = Integer.MAX_VALUE;
    private int curConflicts = Integer.MAX_VALUE;
    private int maxSteps;

    private Variable<T> lastVar;

    public GreedyDescent(CSP<T> csp, LocalMinimumReaction localMinimumReaction, SelectionMode selectionMode) {
        this(csp, 10000, localMinimumReaction, selectionMode, false);
    }

    public GreedyDescent(CSP<T> csp, int maxSteps, LocalMinimumReaction localMinimumReaction,
                         SelectionMode selectionMode, boolean isUsingGAC) {
        super(csp);
        this.maxSteps = maxSteps;
        this.localMinimumReaction = localMinimumReaction;
        this.selectionMode = selectionMode;
        if (isUsingGAC) {
            gac = new GAC<>(csp, true);
        }
    }

    @Override
    public void init() {
        curSteps = totalSteps = curMinima = 0;
        minConflicts = Integer.MAX_VALUE;
        hasFinished = false;
        notifyDomainChanged(getCSP().startingAssignment(), getCSP().getVariables());
        if (gac != null) {
            gac.clear();
            setCurrentState(new InitialGACState());
        } else {
            setCurrentState(new InitRunState());
        }
    }

    @Override
    public boolean hasEnded() {
        return hasFinished;
    }

    @Override
    protected AlgorithmState getNormalStepState() {
        return new NormalStepState();
    }

    @Override
    public Map<AlgorithmStatistic, Number> getStatistics() {
        Map<AlgorithmStatistic, Number> stats = new EnumMap<>(AlgorithmStatistic.class);
        stats.put(AlgorithmStatistic.CUR_STEP, curSteps);
        stats.put(AlgorithmStatistic.TOTAL_STEPS, totalSteps);
        if (gac != null) {
            stats.put(AlgorithmStatistic.GAC_ITERATIONS, gac.getIterations());
        } else {
            stats.put(AlgorithmStatistic.GAC_ITERATIONS, 0);
        }
        stats.put(AlgorithmStatistic.CUR_MINIMA_VISITED, curMinima);
        stats.put(AlgorithmStatistic.CUR_CONFLICTS, curConflicts);
        stats.put(AlgorithmStatistic.MIN_CONFLICTS, minConflicts);
        return stats;
    }

    @Override
    public List<AlgorithmStatistic> getAvailableStatistics() {
        return Arrays.asList(AlgorithmStatistic.CUR_STEP,
                AlgorithmStatistic.TOTAL_STEPS,
                AlgorithmStatistic.GAC_ITERATIONS,
                AlgorithmStatistic.CUR_MINIMA_VISITED,
                AlgorithmStatistic.CUR_CONFLICTS,
                AlgorithmStatistic.MIN_CONFLICTS);
    }

    public static List<AlgorithmOptions> getAvailableOptions() {
        return Arrays.asList(
                AlgorithmOptions.INITIAL_GAC,
                AlgorithmOptions.MAX_STEPS,
                AlgorithmOptions.LOCAL_MINIMUM_REACTION,
                AlgorithmOptions.SELECTION_MODE);
    }

    public static List<LocalMinimumReaction> getReactions() {
        return Arrays.asList(
                LocalMinimumReaction.RANDOM_RESTART,
                LocalMinimumReaction.RANDOM_WALK
        );
    }

    public static List<SelectionMode> getSelectionModes() {
        return Arrays.asList(
                SelectionMode.BEST_PAIR,
                SelectionMode.TWO_STEP,
                SelectionMode.ANY_CONFLICT,
                SelectionMode.RANDOM
        );
    }
}
