package mbud.sudokusolver.csp.algorithm.impl;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.CSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.BacktrackingAlgorithm;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmOptions;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmStatistic;
import mbud.sudokusolver.csp.algorithm.exception.EmptyDomainException;
import mbud.sudokusolver.csp.algorithm.heuristics.ValueSelectHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.VarSelectHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.impl.FirstValueHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.impl.FirstVarHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.impl.ValueUsageHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.impl.VarRestrictionHeuristic;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class Backtrack<T> extends BacktrackingAlgorithm<T> {
    private class AssignValueState extends BacktrackableState<T> {

        public AssignValueState(Variable<T> variable) {
            super(variable);
            collection = getValuesForVar(variable);
            iterator = collection.iterator();
        }

        @Override
        protected Assignment<T> assignDomainAndCheckConsistency(List<Variable<T>> changedVariablesState) {
            T value = iterator.next();
            sendMessage(MessageType.VARIABLE_ASSIGNED, variable, value);
            changedVariablesState.add(variable);
            Assignment<T> newAssign = getAssignment().createAssignmentWithNewValue(variable, value);

            if (isCheckingForward) {
                try {
                    gac.solve(newAssign, variable, changedVariablesState);
                } catch (EmptyDomainException e) {
                    sendMessage(BacktrackingAlgorithm.MessageType.BACKTRACK_GAC_EMPTY_DOMAIN);
                    backtracks++;
                    return null;
                }
            } else {
                if (!getCSP().isConsistent(newAssign, variable)) {
                    sendMessage(MessageType.BACKTRACK_VALUE_NOT_CONSISTENT);
                    backtracks++;
                    return null;
                }
            }

            return newAssign;
        }

        private List<T> getValuesForVar(Variable<T> v) {
            return valueSelectHeuristic.getValuesForVariable(getCSP(), getAssignment(), v);
        }
    }

    public enum MessageType {
        VARIABLE_ASSIGNED, BACKTRACK_VALUE_NOT_CONSISTENT
    }

    private ValueSelectHeuristic<T> valueSelectHeuristic;

    private boolean isCheckingForward = false;

    public Backtrack(CSP<T> csp, boolean isDoingGAC, boolean isCheckingForward) {
        this(csp, isDoingGAC, isCheckingForward, new FirstVarHeuristic<>(), new FirstValueHeuristic<>());
    }

    public Backtrack(CSP<T> csp, boolean isDoingGAC, boolean isCheckingForward, VarSelectHeuristic<T> varHeuristic, ValueSelectHeuristic<T> valueHeuristic) {
        super(csp);
        this.isCheckingForward = isCheckingForward;
        this.varSelectHeuristic = varHeuristic;
        this.valueSelectHeuristic = valueHeuristic;
        if (isDoingGAC || isCheckingForward) {
            gac = new GAC<>(csp);
        }
    }

    public Backtrack(CSP<T> csp, boolean isDoingGAC, boolean isCheckingForward, VarSelectHeuristic.Type varHeuristicType, ValueSelectHeuristic.Type valueHeuristicType) {
        super(csp);
        this.isCheckingForward = isCheckingForward;
        switch (varHeuristicType) {
            case FIRST_VARIABLE:
                this.varSelectHeuristic = new FirstVarHeuristic<>();
                break;
            case MOST_RESTRICTED_VARIABLE:
                this.varSelectHeuristic = new VarRestrictionHeuristic<>(VarRestrictionHeuristic.RestrictionType.MOST_RESTRICTED);
                break;
            case LEAST_RESTRICTED_VARIABLE:
                this.varSelectHeuristic = new VarRestrictionHeuristic<>(VarRestrictionHeuristic.RestrictionType.LEAST_RESTRICTED);
                break;
        }
        switch (valueHeuristicType) {
            case FIRST_VALUE:
                this.valueSelectHeuristic = new FirstValueHeuristic<>();
                break;
            case MOST_USED_VALUE:
                this.valueSelectHeuristic = new ValueUsageHeuristic<>(ValueUsageHeuristic.UsageType.MOST_USED);
                break;
            case LEAST_USED_VALUE:
                this.valueSelectHeuristic = new ValueUsageHeuristic<>(ValueUsageHeuristic.UsageType.LEAST_USED);
                break;
        }
        if (isDoingGAC || isCheckingForward) {
            gac = new GAC<>(csp);
        }
    }

    @Override
    protected BacktrackableState getBacktrackableState(Variable<T> variable) {
        return new AssignValueState(variable);
    }

    @Override
    public void init() {
        super.init();
        if (gac != null) {
            setCurrentState(new InitialGACState());
        } else {
            setCurrentState(new SelectVariableState());
        }
    }

    @Override
    public Map<AlgorithmStatistic, Number> getStatistics() {
        Map<AlgorithmStatistic, Number> stats = new EnumMap<>(AlgorithmStatistic.class);
        stats.put(AlgorithmStatistic.BACKTRACKS, getBacktracks());
        stats.put(AlgorithmStatistic.VISITED_NODES, getIterations());
        stats.put(AlgorithmStatistic.TREE_HEIGHT, curTreeHeight);
        stats.put(AlgorithmStatistic.GAC_ITERATIONS, getGACIterations());
        return stats;
    }

    @Override
    public List<AlgorithmStatistic> getAvailableStatistics() {
        return Arrays.asList(AlgorithmStatistic.VISITED_NODES, AlgorithmStatistic.BACKTRACKS, AlgorithmStatistic.TREE_HEIGHT, AlgorithmStatistic.GAC_ITERATIONS);
    }

    public static List<AlgorithmOptions> getAvailableOptions() {
        return Arrays.asList(
                AlgorithmOptions.INITIAL_GAC,
                AlgorithmOptions.FORWARD_CHECKING,
                AlgorithmOptions.VARIABLE_HEURISTIC,
                AlgorithmOptions.VALUE_HEURISTIC);
    }
}
