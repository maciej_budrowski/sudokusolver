package mbud.sudokusolver.csp;

import java.util.LinkedList;
import java.util.List;

public class Variable<T> {
    private List<T> domain;

    public Variable() {
        domain = new LinkedList<>();
    }

    public Variable(List<T> domain) {
        this.domain = domain;
    }

    public List<T> getDomain() {
        return domain;
    }

    public void restrictTo(T val) {
        domain.clear();
        domain.add(val);
    }

    public void restrictTo(List<T> values) {
        domain.clear();
        domain.addAll(values);
    }

    @Override
    public String toString() {
        return super.toString() + " with domain: " + domain;
    }
}
