package mbud.sudokusolver.control.event;

public interface CanvasClickListener {
    void onSquareClicked(int x, int y);
}
