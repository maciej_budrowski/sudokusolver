package mbud.sudokusolver.control;

import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;

public class CanvasHolder extends Pane {
    private boolean isDragging = false;
    private double lastX;
    private double lastY;
    private double curShiftX;
    private double curShiftY;
    private double curScale = 1;

    public CanvasHolder(Canvas canvas) {
        super(canvas);

        setOnMouseDragged(e -> {
            if (!isDragging) {
                return;
            }
            curShiftX -= lastX - e.getX();
            curShiftY -= lastY - e.getY();
            canvas.relocate(curShiftX, curShiftY);
            lastX = e.getX();
            lastY = e.getY();
        });

        setOnMousePressed(e -> {
            isDragging = true;
            lastX = e.getX();
            lastY = e.getY();
        });

        setOnMouseReleased(e -> isDragging = false);

        setOnScroll(e -> {
            curScale *= Math.pow(1.05, e.getDeltaY() / 40.0);
            canvas.setScaleX(curScale);
            canvas.setScaleY(curScale);
        });
    }
}
