package mbud.sudokusolver.control;

import javafx.scene.control.TextArea;
import javafx.scene.text.Font;

public class MessageLog extends TextArea {
    private final int maxMessages;
    private final int messageCountOnRevert;

    private String[] messages;
    private int messagePointer;
    private int nextMessageIdx;
    private int length;

    public MessageLog(int maxMessages, int messageCountOnRevert) {
        this.maxMessages = maxMessages;
        this.messageCountOnRevert = messageCountOnRevert;

        setFont(Font.font("Courier New"));
        setEditable(false);

        messages = new String[maxMessages];
        messagePointer = length = 0;
        nextMessageIdx = 0;
    }

    public void addMessage(String message) {
        messages[nextMessageIdx++] = message;
        nextMessageIdx %= maxMessages;
        length++;

        if (length >= maxMessages) {
            length = messageCountOnRevert;
            messagePointer = (nextMessageIdx + maxMessages - length) % maxMessages;
            rebuildLog();
        } else {
            appendMessage(message);
        }
    }

    private void appendMessage(String message) {
        appendText("\n");
        appendText(message);
    }

    private void rebuildLog() {
        setText("");
        for (int i = 0; i < length; i++) {
            int idx = (messagePointer + i) % maxMessages;
            appendMessage(messages[idx]);
        }
    }

    @Override
    public void clear() {
        messagePointer = length = nextMessageIdx = 0;
        super.clear();
    }
}
