package mbud.sudokusolver.control;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import mbud.sudokusolver.control.event.CanvasClickListener;
import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.SudokuCSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.model.BasicSudokuModel;
import mbud.sudokusolver.model.KillerSudokuModel;
import mbud.sudokusolver.model.SamuraiSudokuModel;
import mbud.sudokusolver.model.SudokuModel;
import mbud.sudokusolver.util.Pair;

import java.util.ArrayList;
import java.util.List;

public abstract class SudokuCanvas extends Canvas {

    public static final int CANVAS_OFFSET = 25;

    protected GraphicsContext graphicsContext;

    protected int width;
    protected int height;
    protected int sizePerSquare;
    protected int fontSize;
    protected double calculatedWidth;
    protected double calculatedHeight;

    protected Assignment<Integer> assignment;
    protected SudokuCSP csp;
    protected SudokuModel model;

    private Font foldNormal;
    private Font fontBold;
    private boolean lastBold = true;

    private List<CanvasClickListener> canvasClickListeners = new ArrayList<>();
    private List<CanvasClickListener> canvasRightClickListeners = new ArrayList<>();

    private long clickTime;
    private double clickX;
    private double clickY;
    private long clickRightTime;
    private double clickRightX;
    private double clickRightY;

    public SudokuCanvas(SudokuModel model, int sizePerSquare, int fontSize) {
        super(sizePerSquare * model.getWidth() + CANVAS_OFFSET * 2, sizePerSquare * model.getHeight() + CANVAS_OFFSET * 2);
        this.width = model.getWidth();
        this.height = model.getHeight();
        calculatedWidth = sizePerSquare * (width + 1);
        calculatedHeight = sizePerSquare * (height + 1);
        this.model = model;

        graphicsContext = getGraphicsContext2D();
        foldNormal = Font.font("Arial", fontSize);
        fontBold = Font.font("Arial", FontWeight.BOLD, fontSize);
        graphicsContext.setFont(fontBold);
        graphicsContext.translate(CANVAS_OFFSET, CANVAS_OFFSET);

        this.sizePerSquare = sizePerSquare;
        this.fontSize = fontSize;

        setOnMousePressed(e -> {
            switch (e.getButton()) {
                case PRIMARY:
                    clickTime = System.currentTimeMillis();
                    clickX = e.getX();
                    clickY = e.getY();
                    break;
                case SECONDARY:
                    clickRightTime = System.currentTimeMillis();
                    clickRightX = e.getX();
                    clickRightY = e.getY();
                    break;
                default:
                    break;
            }
        });

        setOnMouseReleased(e -> {
            switch (e.getButton()) {
                case PRIMARY:
                    if ((System.currentTimeMillis() - clickTime) < 200 && Math.abs(e.getX() - clickX) < 5.0 && Math.abs(e.getY() - clickY) < 5.0) {
                        handleClick(e);
                    }
                    break;
                case SECONDARY:
                    if ((System.currentTimeMillis() - clickRightTime) < 200 && Math.abs(e.getX() - clickRightX) < 5.0 && Math.abs(e.getY() - clickRightY) < 5.0) {
                        handleClick(e);
                    }
                    break;
                default:
                    break;
            }
        });
    }

    private void handleClick(MouseEvent event) {
        double x = event.getX() - CANVAS_OFFSET;
        double y = event.getY() - CANVAS_OFFSET;
        int fieldX = ((int) y / sizePerSquare) + 1;
        int fieldY = ((int) x / sizePerSquare) + 1;
        if (model.contains(fieldX, fieldY)) {
            if (event.getButton() == MouseButton.PRIMARY) {
                onFieldClicked(fieldX, fieldY);
            } else {
                onFieldRightClicked(fieldX, fieldY);
            }
        }
    }

    public void setupBoard() {
        graphicsContext.clearRect(0, 0, getWidth(), getHeight());
        preDrawingLines();
        graphicsContext.setLineWidth(2);
        drawThinLines();
        graphicsContext.setLineWidth(5);
        drawThickLines();
    }

    public void preDrawingLines() {

    }

    public abstract void drawThinLines();

    public abstract void drawThickLines();

    public void drawText(int x, int y, String text, boolean bold) {
        if (bold != lastBold) {
            if (bold) {
                graphicsContext.setFont(fontBold);
                lastBold = true;
            } else {
                graphicsContext.setFont(foldNormal);
                lastBold = false;
            }
        }
        float width = com.sun.javafx.tk.Toolkit.getToolkit().getFontLoader().computeStringWidth(text, graphicsContext.getFont());
        graphicsContext.fillText(text, (sizePerSquare / 2) + (sizePerSquare * (y - 1)) - (width / 2), (sizePerSquare / 2) + (sizePerSquare * (x - 1)) + (fontSize / 2), sizePerSquare - 10);
    }

    public void onFieldClicked(int x, int y) {
        for (CanvasClickListener listener : canvasClickListeners) {
            listener.onSquareClicked(x, y);
        }
    }

    public void onFieldRightClicked(int x, int y) {
        for (CanvasClickListener listener : canvasRightClickListeners) {
            listener.onSquareClicked(x, y);
        }
    }

    public void setCsp(SudokuCSP csp) {
        this.csp = csp;
    }

    public void setAssignment(Assignment<Integer> assignment) {
        this.assignment = assignment;
    }

    public void drawVariables() {
        drawVariables(csp.getChangeableVariables(), false);
    }

    public void drawVariables(boolean bold) {
        for (int x = 1; x <= width; x++) {
            for (int y = 1; y <= height; y++) {
                drawVariable(x, y, bold);
            }
        }
    }

    public void drawVariables(List<Variable<Integer>> variables, boolean bold) {
        for (Variable<Integer> v : variables) {
            drawVariable(v, bold);
        }
    }

    public void drawVariable(int x, int y, boolean bold) {
        List<Integer> domain = assignment.getDomainForVar(csp.valueAt(x, y));
        clearTile(x, y);
        if (domain.size() != 1) {
            return;
        }
        drawText(x, y, domain.get(0).toString(), bold);
    }

    public void clearTile(int x, int y) {
        graphicsContext.clearRect(((y - 1) * sizePerSquare) + 10, ((x - 1) * sizePerSquare) + 10, sizePerSquare - 20, sizePerSquare - 20);
    }

    public void drawVariable(Variable<Integer> variable) {
        drawVariables(false);
    }

    public void drawVariable(Variable<Integer> variable, boolean bold) {
        Pair<Integer, Integer> coord = csp.coordOfVar(variable);
        drawVariable(coord.getA(), coord.getB(), bold);
    }

    public void registerClickListener(CanvasClickListener listener) {
        canvasClickListeners.add(listener);
    }

    public void registerRightClickListener(CanvasClickListener listener) {
        canvasRightClickListeners.add(listener);
    }

    public static SudokuCanvas createFrom(SudokuModel model, int sizePerSquare, int fontSize) {
        if (model instanceof KillerSudokuModel) {
            return new KillerSudokuCanvas((KillerSudokuModel) model, sizePerSquare, fontSize);
        }
        if (model instanceof SamuraiSudokuModel) {
            return new SamuraiSudokuCanvas((SamuraiSudokuModel) model, sizePerSquare, fontSize);
        }
        return new BasicSudokuCanvas((BasicSudokuModel) model, sizePerSquare, fontSize);
    }
}
