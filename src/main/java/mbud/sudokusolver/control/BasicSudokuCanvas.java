package mbud.sudokusolver.control;

import mbud.sudokusolver.model.BasicSudokuModel;

public class BasicSudokuCanvas extends SudokuCanvas {
    private BasicSudokuModel basicSudokuModel;

    public BasicSudokuCanvas(BasicSudokuModel basicSudokuModel, int sizePerSquare, int fontSize) {
        super(basicSudokuModel, sizePerSquare, fontSize);
        this.basicSudokuModel = basicSudokuModel;
    }

    @Override
    public void drawThinLines() {
        for (int i = 0; i <= width; i++) {
            graphicsContext.strokeLine(sizePerSquare * i, 0, sizePerSquare * i, width * sizePerSquare);
            graphicsContext.strokeLine(0, sizePerSquare * i, width * sizePerSquare, sizePerSquare * i);
        }
    }

    @Override
    public void drawThickLines() {
        for (int i = 0; i <= basicSudokuModel.getBoxHeight(); i++) {
            graphicsContext.strokeLine(i * basicSudokuModel.getBoxWidth() * sizePerSquare, 0, i * basicSudokuModel.getBoxWidth() * sizePerSquare, width * sizePerSquare);
        }
        for (int i = 0; i <= basicSudokuModel.getBoxWidth(); i++) {
            graphicsContext.strokeLine(0, i * basicSudokuModel.getBoxHeight() * sizePerSquare, width * sizePerSquare, i * basicSudokuModel.getBoxHeight() * sizePerSquare);
        }
    }
}
