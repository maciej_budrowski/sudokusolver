package mbud.sudokusolver.control;

import mbud.sudokusolver.model.SamuraiSudokuModel;

public class SamuraiSudokuCanvas extends SudokuCanvas {
    public SamuraiSudokuCanvas(SamuraiSudokuModel samuraiSudokuModel, int sizePerSquare, int fontSize) {
        super(samuraiSudokuModel, sizePerSquare, fontSize);
    }

    @Override
    public void drawThinLines() {
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 10; i++) {
                graphicsContext.strokeLine(sizePerSquare * i + (12 * sizePerSquare) * (j % 2), (12 * sizePerSquare) * (j / 2), sizePerSquare * i + (12 * sizePerSquare) * (j % 2), (9 * sizePerSquare) + (12 * sizePerSquare) * (j / 2));
                graphicsContext.strokeLine((12 * sizePerSquare) * (j % 2), sizePerSquare * i + (12 * sizePerSquare) * (j / 2), (9 * sizePerSquare) + (12 * sizePerSquare) * (j % 2), sizePerSquare * i + (12 * sizePerSquare) * (j / 2));
            }
        }
        for (int i = 0; i < 10; i++) {
            graphicsContext.strokeLine(sizePerSquare * i + (6 * sizePerSquare), (6 * sizePerSquare), sizePerSquare * i + (6 * sizePerSquare), (9 * sizePerSquare) + (6 * sizePerSquare));
            graphicsContext.strokeLine((6 * sizePerSquare), sizePerSquare * i + (6 * sizePerSquare), (9 * sizePerSquare) + (6 * sizePerSquare), sizePerSquare * i + (6 * sizePerSquare));
        }
    }

    @Override
    public void drawThickLines() {
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 4; i++) {
                graphicsContext.strokeLine((3 * sizePerSquare) * i + (12 * sizePerSquare) * (j % 2), (12 * sizePerSquare) * (j / 2), (3 * sizePerSquare) * i + (12 * sizePerSquare) * (j % 2), (9 * sizePerSquare) + (12 * sizePerSquare) * (j / 2));
                graphicsContext.strokeLine((12 * sizePerSquare) * (j % 2), (3 * sizePerSquare) * i + (12 * sizePerSquare) * (j / 2), (9 * sizePerSquare) + (12 * sizePerSquare) * (j % 2), (3 * sizePerSquare) * i + (12 * sizePerSquare) * (j / 2));
            }
        }
        for (int i = 0; i < 4; i++) {
            graphicsContext.strokeLine((3 * sizePerSquare) * i + (6 * sizePerSquare), (6 * sizePerSquare), (3 * sizePerSquare) * i + (6 * sizePerSquare), (9 * sizePerSquare) + (6 * sizePerSquare));
            graphicsContext.strokeLine((6 * sizePerSquare), (3 * sizePerSquare) * i + (6 * sizePerSquare), (9 * sizePerSquare) + (6 * sizePerSquare), (3 * sizePerSquare) * i + (6 * sizePerSquare));
        }
    }
}
