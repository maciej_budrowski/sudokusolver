package mbud.sudokusolver.control;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import mbud.sudokusolver.model.KillerSudokuModel;
import mbud.sudokusolver.util.Pair;

import java.util.List;

public class KillerSudokuCanvas extends BasicSudokuCanvas {
    //@formatter:off
    private Color[] colors = new Color[] {
        Color.rgb(230, 25, 75), //RED
        Color.rgb(60, 180, 75), //GREEN
        Color.rgb(255, 225, 25), //YELLOW
        Color.rgb(0, 130, 200), //BLUE
        Color.rgb(245, 130, 48), //ORANGE
        Color.rgb(145, 30, 180), //PURPLE
        Color.rgb(70, 240, 240), //CYAN
        Color.rgb(240, 50, 230), //MAGENTA
        Color.rgb(210, 245, 60), //LIME
        Color.rgb(250, 190, 190), //PINK
        Color.rgb(0, 128, 128), //TEAL
        Color.rgb(230, 190, 255), //LAVENDER
        Color.rgb(170, 110, 40), //BROWN
        Color.rgb(255, 250, 200), //BEIGE
        Color.rgb(128, 0, 0), //MAROON
        Color.rgb(170, 255, 195), //MINT
        Color.rgb(128, 128, 0), //OLIVE
        Color.rgb(255, 215, 180), //CORAL
        Color.rgb(128, 128, 128) //GREY
    };
    //@formatter:on

    private List<Pair<List<Pair<Integer, Integer>>, Integer>> groups;
    private Font sizeFont;

    public KillerSudokuCanvas(KillerSudokuModel killerSudokuModel, int sizePerSquare, int fontSize) {
        super(killerSudokuModel, sizePerSquare, fontSize);

        this.groups = killerSudokuModel.getGroups();
        sizeFont = Font.font("Arial", fontSize * 2 / 3);
    }

    @Override
    public void preDrawingLines() {
        for (int i = 0; i < groups.size(); i++) {
            drawGroup(groups.get(i), colors[i % colors.length]);
        }
    }

    private void drawGroup(Pair<List<Pair<Integer, Integer>>, Integer> group, Color color) {
        Paint oldPaint = graphicsContext.getFill();
        for (int i = 0; i < group.getA().size(); i++) {
            Pair<Integer, Integer> square = group.getA().get(i);
            graphicsContext.setFill(color);
            fillSquareColor(square);
            if (i == 0) {
                drawGroupSizeAt(square.getA(), square.getB(), group.getB());
            }
        }
        graphicsContext.setFill(oldPaint);
    }

    private void fillSquareColor(Pair<Integer, Integer> square) {
        graphicsContext.fillRect((square.getB() - 1) * sizePerSquare, (square.getA() - 1) * sizePerSquare, sizePerSquare, sizePerSquare);
    }

    @Override
    public void clearTile(int x, int y) {
        Color color = getColorForTile(x, y);
        if (color == null) {
            graphicsContext.clearRect(((y - 1) * sizePerSquare) + 10, ((x - 1) * sizePerSquare) + 10, sizePerSquare - 20, sizePerSquare - 20);
        } else {
            Paint oldPaint = graphicsContext.getFill();
            graphicsContext.setFill(color);
            graphicsContext.fillRect(((y - 1) * sizePerSquare) + 10, ((x - 1) * sizePerSquare) + 10, sizePerSquare - 20, sizePerSquare - 20);
            graphicsContext.setFill(oldPaint);
            Integer groupSize = getSizeForFirstGroupElement(x, y);
            if (groupSize != null) {
                drawGroupSizeAt(x, y, groupSize);
            }
        }
    }

    private Integer getSizeForFirstGroupElement(int x, int y) {
        for (Pair<List<Pair<Integer, Integer>>, Integer> group : groups) {
            Pair<Integer, Integer> firstSquare = group.getA().get(0);
            if (firstSquare.getA().equals(x) && firstSquare.getB().equals(y)) {
                return group.getB();
            }
        }
        return null;
    }

    public Color getColorForTile(int x, int y) {
        for (int i = 0; i < groups.size(); i++) {
            for (Pair<Integer, Integer> square : groups.get(i).getA()) {
                if (square.getA().equals(x) && square.getB().equals(y)) {
                    return colors[i % colors.length];
                }
            }
        }
        return null;
    }

    private void drawGroupSizeAt(int x, int y, int size) {
        Font font = graphicsContext.getFont();
        Paint paint = graphicsContext.getFill();
        graphicsContext.setFill(Color.BLACK);
        graphicsContext.setFont(sizeFont);
        graphicsContext.fillText(Integer.toString(size), (y - 1) * sizePerSquare, (x - 1) * sizePerSquare + sizeFont.getSize());
        graphicsContext.setFont(font);
        graphicsContext.setFill(paint);
    }
}
