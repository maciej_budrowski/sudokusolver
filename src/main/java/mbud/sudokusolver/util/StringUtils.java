package mbud.sudokusolver.util;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class StringUtils {
    private static Map<Integer, DecimalFormat> decimalFormatMap = new HashMap<>();

    private StringUtils() {
    }

    public static String formatNumber(Number number, int precision) {
        DecimalFormat format = decimalFormatMap.get(precision);
        if (format == null) {
            if (precision == 0) {
                format = new DecimalFormat("0");
            } else {
                StringBuilder sb = new StringBuilder("0.");
                for (int i = 0; i < precision; i++) {
                    sb.append("0");
                }
                format = new DecimalFormat(sb.toString());
            }
            decimalFormatMap.put(precision, format);
        }
        return format.format(number);
    }
}
