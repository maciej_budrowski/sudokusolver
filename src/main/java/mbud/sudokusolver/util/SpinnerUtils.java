package mbud.sudokusolver.util;

import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.util.StringConverter;

public class SpinnerUtils {
    private SpinnerUtils() {
    }

    public static Spinner<Integer> createSpinner(int min, int max, int initialValue, int amountToStepBy) {
        Spinner<Integer> spinner = new Spinner<>(min, max, initialValue, amountToStepBy);
        spinner.getEditor().textProperty().addListener((obs, oldVal, newVal) -> {
            SpinnerValueFactory<Integer> valueFactory = spinner.getValueFactory();
            if (valueFactory != null) {
                StringConverter<Integer> converter = valueFactory.getConverter();
                if (converter != null) {
                    try {
                        Integer value = converter.fromString(newVal);
                        if (value != null)
                            valueFactory.setValue(value);
                        else
                            valueFactory.setValue(0);
                    } catch (NumberFormatException ex) {
                        spinner.getEditor().setText(converter.toString(valueFactory.getValue()));
                    }
                }
            }
        });
        spinner.setEditable(true);
        return spinner;
    }

    public static Spinner<Double> createSpinner(double min, double max, double initialValue, double amountToStepBy) {
        Spinner<Double> spinner = new Spinner<>(min, max, initialValue, amountToStepBy);
        spinner.getEditor().textProperty().addListener((obs, oldVal, newVal) -> {
            SpinnerValueFactory<Double> valueFactory = spinner.getValueFactory();
            if (valueFactory != null) {
                StringConverter<Double> converter = valueFactory.getConverter();
                if (converter != null) {
                    try {
                        Double value = converter.fromString(newVal);
                        if (value != null)
                            valueFactory.setValue(value);
                        else
                            valueFactory.setValue(0.0);
                    } catch (NumberFormatException ex) {
                        spinner.getEditor().setText(converter.toString(valueFactory.getValue()));
                    }
                }
            }
        });
        spinner.setEditable(true);
        return spinner;
    }
}
