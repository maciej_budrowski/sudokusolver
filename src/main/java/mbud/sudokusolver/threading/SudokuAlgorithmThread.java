package mbud.sudokusolver.threading;

import mbud.sudokusolver.csp.algorithm.SolvingAlgorithm;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmStatistic;
import mbud.sudokusolver.csp.algorithm.event.AlgorithmEndListener;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SudokuAlgorithmThread {

    public enum RepeatConditions {
        ALWAYS_REPEAT, REPEAT_UNTIL_SOLUTION
    }

    private class PausableRunnable implements Runnable {
        private volatile boolean hasStarted = false;
        private boolean running = false;
        private volatile boolean paused = false;
        private volatile boolean stepOnce = false;
        private final Object pauseLock = new Object();
        private SolvingAlgorithm<Integer> algorithm;

        public PausableRunnable(SolvingAlgorithm<Integer> algorithm) {
            this.algorithm = algorithm;
        }

        @Override
        public void run() {
            synchronized (testLock) {
                timeStarted = System.currentTimeMillis();
                algorithm.init();
                while (running && !algorithm.hasEnded() && !thread.isInterrupted()) {
                    synchronized (pauseLock) {
                        if (!running) {
                            break;
                        }
                        if (paused) {
                            try {
                                pauseLock.wait();
                            } catch (InterruptedException ex) {
                                break;
                            }
                            if (!running) {
                                break;
                            }
                        }
                    }
                    synchronized (algorithm) {
                        algorithm.step();
                    }
                    if (!noPause) {
                        try {
                            Thread.sleep(interval);
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                    if (stepOnce) {
                        stepOnce = false;
                        pause();
                    }
                }
                timeElapsed = System.currentTimeMillis() - timeStarted;
                if (algorithm.hasEnded()) {
                    for (AlgorithmEndListener<Integer> listener : algorithmEndListenerList) {
                        listener.onAlgorithmEnded(algorithm.getAssignment(), getStatistics());
                    }
                }
                runnable = null;
                thread = null;
            }
        }

        public void start() {
            synchronized (pauseLock) {
                hasStarted = true;
                running = true;
                paused = false;
                pauseLock.notifyAll();
            }
        }

        public void stop() {
            synchronized (pauseLock) {
                running = false;
                paused = false;
                prematureAbort = true;
                thread.interrupt();
                pauseLock.notifyAll();
            }
        }

        public void pause() {
            synchronized (pauseLock) {
                paused = true;
            }
        }

        public void resume() {
            synchronized (pauseLock) {
                paused = false;
                pauseLock.notifyAll();
            }
        }

        public void step() {
            synchronized (pauseLock) {
                if (!hasStarted) {
                    hasStarted = true;
                    running = true;
                }
                stepOnce = true;
                paused = false;
                pauseLock.notifyAll();
            }
        }
    }

    private SolvingAlgorithm<Integer> algorithm;

    private PausableRunnable runnable;
    private Thread thread;
    private boolean noPause = false;
    private int interval = 25;
    private long timeStarted;
    private long timeElapsed = -1;

    private int repeats = 5;
    private int completedRepeats = 0;
    private boolean prematureAbort = false;
    private RepeatConditions repeatCondition = RepeatConditions.REPEAT_UNTIL_SOLUTION;
    private Set<AlgorithmEndListener<Integer>> algorithmEndListenerList = new HashSet<>();

    private final Object testLock = new Object();

    public SudokuAlgorithmThread(SolvingAlgorithm<Integer> algorithm) {
        this.algorithm = algorithm;
    }

    private void init() {
        prematureAbort = false;
        algorithm.setEventsEnabled(!noPause);
        runnable = new PausableRunnable(algorithm);
        thread = new Thread(runnable);
    }

    public boolean startRepeatIfNeeded() {
        if (prematureAbort) {
            return false;
        }

        completedRepeats++;
        if (repeatCondition == RepeatConditions.REPEAT_UNTIL_SOLUTION && algorithm.getAssignment() != null && algorithm.getCSP().isSatisfied(algorithm.getAssignment())) {
            return false;
        }
        if (completedRepeats < repeats) {
            start();
            return true;
        }
        return false;
    }

    public void start() {
        synchronized (testLock) {
            if (thread == null) {
                init();
            }

            if (thread.isAlive()) {
                resume();
            } else {
                thread.start();
                runnable.start();
            }
        }
    }

    public void resume() {
        if (runnable == null) {
            throw new IllegalStateException("Algorithm hasn't started!");
        }

        runnable.resume();
    }

    public void stop() {
        if (runnable == null) {
            throw new IllegalStateException("Algorithm hasn't started!");
        }

        runnable.stop();
    }

    public void pause() {
        if (runnable == null) {
            throw new IllegalStateException("Algorithm hasn't started!");
        }

        runnable.pause();
    }

    public void step() {
        if (thread == null) {
            init();
        }

        if (!thread.isAlive()) {
            runnable.pause();
            thread.start();
        }
        runnable.step();
    }

    public void setRepeats(int repeats) {
        this.repeats = repeats;
    }

    public int getRepeats() {
        return repeats;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public void setNoPause(boolean noPause) {
        this.noPause = noPause;
        algorithm.setEventsEnabled(!noPause);
    }

    public SolvingAlgorithm<Integer> getAlgorithm() {
        return algorithm;
    }

    public boolean isRunning() {
        return (thread != null && thread.isAlive());
    }

    public void setRepeatCondition(RepeatConditions repeatCondition) {
        this.repeatCondition = repeatCondition;
    }

    public Map<AlgorithmStatistic, Number> getStatistics() {
        Map<AlgorithmStatistic, Number> statistics = algorithm.getStatistics();
        statistics.put(AlgorithmStatistic.TIME_PASSED, timeElapsed);
        statistics.put(AlgorithmStatistic.CUR_RESTART, completedRepeats);
        statistics.put(AlgorithmStatistic.TOTAL_RESTARTS, repeats);
        return statistics;
    }

    public void registerAlgorithmEndListener(AlgorithmEndListener<Integer> listener) {
        algorithmEndListenerList.add(listener);
    }

    public void clear() {
        synchronized (algorithm) {
            algorithm.init();
        }
    }
}