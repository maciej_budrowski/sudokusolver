package mbud.sudokusolver.model;

import mbud.sudokusolver.csp.SudokuCSP;
import mbud.sudokusolver.csp.model.SudokuCSPCreator;
import mbud.sudokusolver.csp.model.impl.BasicSudokuCSPCreator;

public class BasicSudokuModel extends SudokuModel {

    private int boxWidth;
    private int boxHeight;

    public BasicSudokuModel(int boxWidth, int boxHeight) {
        super(boxWidth * boxHeight, boxWidth * boxHeight);

        this.boxHeight = boxHeight;
        this.boxWidth = boxWidth;
    }

    public int getBoxHeight() {
        return boxHeight;
    }

    public int getBoxWidth() {
        return boxWidth;
    }

    @Override
    public SudokuCSP toCSP() {
        SudokuCSPCreator creator = new BasicSudokuCSPCreator(boxWidth, boxHeight);
        SudokuCSP csp = new SudokuCSP(creator);
        for (int x = 1; x <= height; x++) {
            for (int y = 1; y <= width; y++) {
                int val = getValueAt(x, y);
                if (val == 0) {
                    continue;
                }
                csp.assignStartingValue(x, y, val);
            }
        }
        return csp;
    }

    @Override public SudokuType getType() {
        return SudokuType.SUDOKU;
    }

    @Override public int getMaxValue() {
        return width;
    }
}
