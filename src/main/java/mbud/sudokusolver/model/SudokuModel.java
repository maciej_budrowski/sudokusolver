package mbud.sudokusolver.model;

import mbud.sudokusolver.csp.SudokuCSP;

public abstract class SudokuModel {

    private int[][] values;
    protected int width;
    protected int height;

    public SudokuModel(int width, int height) {
        values = new int[width][height];

        this.width = width;
        this.height = height;
    }

    public void setValue(int x, int y, int value) {
        values[x - 1][y - 1] = value;
    }

    public int getValueAt(int x, int y) {
        return values[x - 1][y - 1];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean contains(int x, int y) {
        return x > 0 && x <= width && y > 0 && y <= height;
    }

    public abstract SudokuCSP toCSP();

    public abstract SudokuType getType();

    public abstract int getMaxValue();
}
