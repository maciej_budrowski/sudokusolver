package mbud.sudokusolver.model;

public enum SudokuType {
    SUDOKU, KILLER_SUDOKU, SAMURAI_SUDOKU
}
