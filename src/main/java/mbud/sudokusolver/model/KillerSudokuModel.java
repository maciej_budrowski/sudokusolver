package mbud.sudokusolver.model;

import mbud.sudokusolver.csp.SudokuCSP;
import mbud.sudokusolver.csp.model.impl.KillerSudokuCSPCreator;
import mbud.sudokusolver.util.Pair;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class KillerSudokuModel extends BasicSudokuModel {

    private List<Pair<List<Pair<Integer, Integer>>, Integer>> groups = new ArrayList<>();
    private static Comparator<Pair<Integer, Integer>> comparator = (o1, o2) -> {
        if (o1.getA() < o2.getA()) {
            return -1;
        }
        if (o2.getA() < o1.getA()) {
            return 1;
        }
        if (o1.getB() < o2.getB()) {
            return -1;
        }
        if (o2.getB() < o1.getB()) {
            return 1;
        }
        return 0;
    };

    public KillerSudokuModel() {
        super(3, 3);
    }

    public Pair<List<Pair<Integer, Integer>>, Integer> addGroup(int sum, int... coord) {
        if (coord.length <= 0 || coord.length % 2 == 1 || sum < 0 || sum < coord.length) {
            return null;
        }
        List<Pair<Integer, Integer>> listOfPairs = new ArrayList<>();
        for (int i = 0; i < coord.length; i += 2) {
            listOfPairs.add(new Pair<>(coord[i], coord[i + 1]));
        }
        listOfPairs.sort(comparator);
        Pair<List<Pair<Integer, Integer>>, Integer> group = new Pair<>(listOfPairs, sum);
        groups.add(group);
        return group;
    }

    public List<Pair<List<Pair<Integer, Integer>>, Integer>> getGroups() {
        return groups;
    }

    public void sortGroupMembers() {
        for (Pair<List<Pair<Integer, Integer>>, Integer> group : groups) {
            group.getA().sort(comparator);
        }
    }

    @Override
    public SudokuCSP toCSP() {
        KillerSudokuCSPCreator creator = new KillerSudokuCSPCreator();
        creator.setGroups(groups);
        SudokuCSP csp = new SudokuCSP(creator);
        for (int x = 1; x <= 9; x++) {
            for (int y = 1; y <= 9; y++) {
                int val = getValueAt(x, y);
                if (val == 0) {
                    continue;
                }
                csp.assignStartingValue(x, y, val);
            }
        }
        return csp;
    }

    @Override public SudokuType getType() {
        return SudokuType.KILLER_SUDOKU;
    }
}
