package mbud.sudokusolver.model;

import mbud.sudokusolver.csp.SudokuCSP;
import mbud.sudokusolver.csp.model.SudokuCSPCreator;
import mbud.sudokusolver.csp.model.impl.SamuraiSudokuCSPCreator;

public class SamuraiSudokuModel extends SudokuModel {
    public SamuraiSudokuModel() {
        super(21, 21);
    }

    @Override
    public SudokuCSP toCSP() {
        SudokuCSPCreator creator = new SamuraiSudokuCSPCreator();
        SudokuCSP csp = new SudokuCSP(creator);
        for (int x = 1; x <= 21; x++) {
            for (int y = 1; y <= 21; y++) {
                if ((x < 7 || x > 15) && (y > 9 && y < 13)) {
                    continue;
                }
                if ((y < 7 || y > 15) && (x > 9 && x < 13)) {
                    continue;
                }
                int val = getValueAt(x, y);
                if (val == 0) {
                    continue;
                }
                csp.assignStartingValue(x, y, val);
            }
        }
        return csp;
    }

    @Override
    public boolean contains(int x, int y) {
        if ((x > 9 && x < 13) && (y < 7 || y > 15)) {
            return false;
        }

        if ((y > 9 && y < 13) && (x < 7 || x > 15)) {
            return false;
        }

        return super.contains(x, y);
    }

    @Override public SudokuType getType() {
        return SudokuType.SAMURAI_SUDOKU;
    }

    @Override public int getMaxValue() {
        return 9;
    }
}
