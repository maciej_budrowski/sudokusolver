package mbud.sudokusolver;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import mbud.sudokusolver.controller.MainWindowController;

import java.util.Locale;
import java.util.ResourceBundle;

public class SudokuSolverApplication extends Application {

    private MainWindowController mainWindow;

    public static void main(String[] args) {
        //TODO: Naprawić wyświetlanie języka we wbudowanych kontrolkach np. cancel w dialogu
        Locale.setDefault(Locale.forLanguageTag("pl"));
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(SudokuSolverApplication.class.getClassLoader().getResource("layout/main_window.fxml"));
        loader.setResources(ResourceBundle.getBundle("language.LangBundle", Locale.forLanguageTag("pl")));
        Parent root = loader.load();
        mainWindow = loader.getController();

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Sudoku solver");
        primaryStage.setOnCloseRequest(this::handleWindowClose);
        primaryStage.show();
    }

    public void handleWindowClose(WindowEvent event) {
        mainWindow.onWindowClose();
    }
}
