package mbud.sudokusolver.io;

import mbud.sudokusolver.model.*;
import mbud.sudokusolver.util.Pair;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SudokuIO {
    private SudokuIO() {
    }

    public static SudokuModel readFromFile(File file) throws IOException {
        try (Stream<String> stream = Files.lines(Paths.get(file.getPath()))) {
            List<String> lines = stream.collect(Collectors.toList());
            if (lines == null || lines.isEmpty()) {
                return null;
            }

            Iterator<String> iterator = lines.iterator();
            String header = iterator.next();
            switch (SudokuType.valueOf(header)) {
                case SUDOKU:
                    return readBasicSudoku(iterator);
                case KILLER_SUDOKU:
                    return readKillerSudoku(iterator);
                case SAMURAI_SUDOKU:
                    return readSamuraiSudoku(iterator);
            }
        }

        return null;
    }

    private static BasicSudokuModel readBasicSudoku(Iterator<String> iterator) {
        String buf = iterator.next();
        String[] split = buf.split(" ");
        int width = Integer.parseInt(split[0]);
        int height = Integer.parseInt(split[1]);
        BasicSudokuModel model = new BasicSudokuModel(width, height);
        readBoardContent(iterator, model, width * height, width * height);
        return model;
    }

    private static SamuraiSudokuModel readSamuraiSudoku(Iterator<String> iterator) {
        SamuraiSudokuModel model = new SamuraiSudokuModel();
        readBoardContent(iterator, model, 21, 21);
        return model;
    }

    private static KillerSudokuModel readKillerSudoku(Iterator<String> iterator) {
        KillerSudokuModel model = new KillerSudokuModel();
        readBoardContent(iterator, model, 9, 9);
        String buf;
        String[] split;
        int[] coords;
        int sum;
        while (iterator.hasNext()) {
            buf = iterator.next();
            split = buf.split(" ");
            sum = Integer.parseInt(split[0]);
            coords = new int[split.length - 1];
            for (int i = 1; i < split.length; i++) {
                coords[i - 1] = Integer.parseInt(split[i]);
            }
            model.addGroup(sum, coords);
        }
        return model;
    }

    private static void readBoardContent(Iterator<String> iterator, SudokuModel model, int width, int height) {
        String buf;
        String[] split;
        int val;
        for (int i = 1; i <= height; i++) {
            buf = iterator.next();
            split = buf.split(" ");
            for (int j = 1; j <= width; j++) {
                val = Integer.parseInt(split[j - 1]);
                model.setValue(i, j, val);
            }
        }
    }

    public static void writeToFile(SudokuModel model, File file) throws IOException {
        Class<?> modelClass = model.getClass();
        List<String> lines = new ArrayList<>();
        if (modelClass.equals(BasicSudokuModel.class)) {
            writeBasicModel((BasicSudokuModel) model, lines);
        } else if (modelClass.equals(KillerSudokuModel.class)) {
            writeKillerModel((KillerSudokuModel) model, lines);
        } else if (modelClass.equals(SamuraiSudokuModel.class)) {
            writeSamuraiModel((SamuraiSudokuModel) model, lines);
        } else {
            return;
        }

        Files.write(Paths.get(file.getPath()), lines, StandardOpenOption.CREATE);
    }

    private static void writeBasicModel(BasicSudokuModel model, List<String> lines) {
        lines.add(SudokuType.SUDOKU.toString());
        lines.add(model.getBoxWidth() + " " + model.getBoxHeight());
        writeBoardContent(model, lines);
    }

    private static void writeKillerModel(KillerSudokuModel model, List<String> lines) {
        lines.add(SudokuType.KILLER_SUDOKU.toString());
        writeBoardContent(model, lines);
        List<Pair<List<Pair<Integer, Integer>>, Integer>> groups = model.getGroups();
        StringBuilder sb;
        for (Pair<List<Pair<Integer, Integer>>, Integer> group : groups) {
            sb = new StringBuilder();
            sb.append(group.getB());
            for (Pair<Integer, Integer> coord : group.getA()) {
                sb.append(" ");
                sb.append(coord.getA());
                sb.append(" ");
                sb.append(coord.getB());
            }
            lines.add(sb.toString());
        }
    }

    private static void writeSamuraiModel(SamuraiSudokuModel model, List<String> lines) {
        lines.add(SudokuType.SAMURAI_SUDOKU.toString());
        writeBoardContent(model, lines);
    }

    private static void writeBoardContent(SudokuModel model, List<String> lines) {
        StringBuilder sb;
        for (int x = 1; x <= model.getHeight(); x++) {
            sb = new StringBuilder();
            sb.append(model.getValueAt(x, 1));
            for (int y = 2; y <= model.getWidth(); y++) {
                sb.append(" ");
                sb.append(model.getValueAt(x, y));
            }
            lines.add(sb.toString());
        }
    }
}
