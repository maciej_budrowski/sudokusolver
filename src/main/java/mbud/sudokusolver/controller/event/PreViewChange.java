package mbud.sudokusolver.controller.event;

import mbud.sudokusolver.controller.AbstractController;

public interface PreViewChange {
    void onViewChange(AbstractController controller);
}
