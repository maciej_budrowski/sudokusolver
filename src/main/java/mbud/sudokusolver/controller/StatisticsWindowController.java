package mbud.sudokusolver.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import mbud.sudokusolver.controller.data.HeaderStatisticDTO;
import mbud.sudokusolver.controller.data.StatisticDTO;
import mbud.sudokusolver.controller.data.StatisticsDTO;

import java.net.URL;
import java.util.ResourceBundle;

public class StatisticsWindowController extends AbstractController {

    public static final String STATISTICS_DTO = "STATISTICS_DTO";

    @FXML
    public TableColumn nameCol;

    @FXML
    public TableColumn minCol;

    @FXML
    public TableColumn avgCol;

    @FXML
    public TableColumn medCol;

    @FXML
    public TableColumn maxCol;

    @FXML
    private TableView tableView;

    @FXML
    private VBox headerStatisticsVbox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        nameCol.setCellValueFactory(
                new PropertyValueFactory<StatisticDTO, String>("name")
        );
        minCol.setCellValueFactory(
                new PropertyValueFactory<StatisticDTO, String>("min")
        );
        avgCol.setCellValueFactory(
                new PropertyValueFactory<StatisticDTO, String>("avg")
        );
        medCol.setCellValueFactory(
                new PropertyValueFactory<StatisticDTO, String>("med")
        );
        maxCol.setCellValueFactory(
                new PropertyValueFactory<StatisticDTO, String>("max")
        );
    }

    @Override
    public void initializeAfterLoad() {
        StatisticsDTO statisticsDTO = (StatisticsDTO) controllerData.getParameter(STATISTICS_DTO);

        tableView.setItems(statisticsDTO.getStatistics());

        for (HeaderStatisticDTO statisticDTO : statisticsDTO.getHeaderStatistics()) {
            Label label = new Label(statisticDTO.getName() + ": " + statisticDTO.getVal());
            headerStatisticsVbox.getChildren().add(label);
        }
    }
}
