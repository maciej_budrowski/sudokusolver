package mbud.sudokusolver.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import mbud.sudokusolver.controller.data.ControllerData;
import mbud.sudokusolver.controller.event.PreViewChange;
import mbud.sudokusolver.io.SudokuIO;
import mbud.sudokusolver.model.SudokuModel;
import mbud.sudokusolver.model.SudokuType;
import mbud.sudokusolver.util.SpinnerUtils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class MainWindowController extends AbstractController {

    @FXML
    private BorderPane contentPane;

    private AbstractController currentViewController;
    private AbstractController currentWindowController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
        try {
            loadContentPane("layout/simulation_window.fxml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadContentPane(String layourUrl) throws IOException {
        loadContentPane(layourUrl, this.controllerData, null);
    }

    public void loadContentPane(String layourUrl, ControllerData<Object> controllerData) throws IOException {
        loadContentPane(layourUrl, controllerData, null);
    }

    public void loadContentPane(String layourUrl, PreViewChange runnable) throws IOException {
        loadContentPane(layourUrl, this.controllerData, runnable);
    }

    public void loadContentPane(String layoutUrl, ControllerData<Object> controllerData, PreViewChange runnable) throws IOException {
        if (currentViewController != null) {
            currentViewController.onWindowClose();
            currentViewController = null;
        }

        FXMLLoader loader = new FXMLLoader(MainWindowController.class.getClassLoader().getResource(layoutUrl));
        loader.setResources(ResourceBundle.getBundle("language.LangBundle", Locale.forLanguageTag("pl")));
        Parent root = loader.load();
        currentViewController = loader.getController();
        currentViewController.setMainWindowController(this);
        currentViewController.initData(controllerData);
        if (runnable != null) {
            runnable.onViewChange(currentViewController);
        }
        currentViewController.initializeAfterLoad();
        contentPane.setCenter(root);
    }

    @Override
    public void onWindowClose() {
        currentViewController.onWindowClose();
        currentViewController = null;
    }

    @FXML
    public void loadFile() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(getLocalized("mainWindow.openSudokuFile"));
        fileChooser.setInitialDirectory(new File("."));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(getLocalized("sudokuFile"), "*.sudoku"));
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            loadContentPane("layout/simulation_window.fxml", c -> {
                SimulationWindowController controller = (SimulationWindowController) c;
                try {
                    SudokuModel sudoku = SudokuIO.readFromFile(file);
                    controller.setSudoku(sudoku);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public void openWindow(String title, String layoutUrl) {
        try {
            FXMLLoader loader = new FXMLLoader(MainWindowController.class.getClassLoader().getResource(layoutUrl));
            loader.setResources(ResourceBundle.getBundle("language.LangBundle", Locale.forLanguageTag("pl")));
            Parent root = loader.load();
            currentWindowController = loader.getController();
            currentWindowController.setMainWindowController(this);
            currentWindowController.initData(controllerData);
            currentWindowController.initializeAfterLoad();
            Stage stage = new Stage();
            stage.setTitle(title);
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void saveSudoku(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(getLocalized("mainWindow.saveSudokuFile"));
        fileChooser.setInitialDirectory(new File("."));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(getLocalized("sudokuFile"), "*.sudoku"));
        File file = fileChooser.showSaveDialog(null);
        if (file != null) {
            try {
                SudokuIO.writeToFile(currentViewController.getCurrentSudoku(), file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void newSudoku(ActionEvent actionEvent) {
        Dialog dialog = new Dialog<>();
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        dialog.setTitle(getLocalized("mainWindow.newSudoku"));
        dialog.setHeaderText(getLocalized("mainWindow.chooseTypeSize"));

        VBox vBox = new VBox();

        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.setItems(FXCollections.observableArrayList(Arrays.stream(SudokuType.values()).map(this::getLocalized).collect(Collectors.toList())));
        comboBox.setValue(getLocalized(SudokuType.SUDOKU));

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        Label labelWidth = new Label(getLocalized("width"));
        Spinner<Integer> spinnerWidth = SpinnerUtils.createSpinner(1, 10, 3, 1);
        Label labelHeight = new Label(getLocalized("height"));
        Spinner<Integer> spinnerHeight = SpinnerUtils.createSpinner(1, 10, 3, 1);

        grid.add(labelWidth, 0, 0);
        grid.add(spinnerWidth, 1, 0);
        grid.add(labelHeight, 0, 1);
        grid.add(spinnerHeight, 1, 1);

        comboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (getLocalized(SudokuType.SUDOKU).equals(newValue)) {
                grid.setVisible(true);
            } else {
                grid.setVisible(false);
            }
        });
        vBox.getChildren().addAll(comboBox, grid);
        dialog.getDialogPane().setContent(vBox);
        Optional result = dialog.showAndWait();
        result.ifPresent(v -> {
            if (v != ButtonType.OK) {
                return;
            }

            controllerData.setParameter(CreationWindowController.PARAM_SUDOKU_TYPE, getValueOfEnum(SudokuType.class, comboBox.getValue()));
            controllerData.setParameter(CreationWindowController.PARAM_SUDOKU_WIDTH, spinnerWidth.getValue());
            controllerData.setParameter(CreationWindowController.PARAM_SUDOKU_HEIGHT, spinnerHeight.getValue());
            try {
                loadContentPane("layout/creation_window.fxml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void close() {
        Stage stage = (Stage) contentPane.getScene().getWindow();
        stage.close();
    }

    public void openAboutMe() {
        Dialog dialog = new Dialog<>();
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);
        dialog.setTitle(getLocalized("aboutAuthor"));
        dialog.setHeaderText(getLocalized("aboutAuthor"));
        VBox vBox = new VBox();
        Label label = new Label(getLocalized("aboutAuthor.uniNotice"));
        label.setPadding(new Insets(0, 0, 10, 0));
        vBox.getChildren().add(label);
        vBox.getChildren().add(createTextWithLabel(getLocalized("aboutAuthor.thesis") + " ", "Zastosowanie algorytmów sztucznej inteligencji w różnych odmianach gry Sudoku"));
        vBox.getChildren().add(createTextWithLabel(getLocalized("aboutAuthor.author") + " ", "Maciej Budrowski"));
        vBox.getChildren().add(createTextWithLabel(getLocalized("aboutAuthor.promoter") + " ", "prof. dr hab. Jarosław Stepaniuk"));
        dialog.getDialogPane().setContent(vBox);
        dialog.show();
    }

    private HBox createTextWithLabel(String label, String text) {
        HBox hBox = new HBox();

        Label boldLabel = new Label(label);
        Font font = Font.font(boldLabel.getFont().getFamily(), FontWeight.BOLD, boldLabel.getFont().getSize());
        boldLabel.setFont(font);
        byte[] textBytes = text.getBytes(Charset.forName("cp1250"));
        Label textLabel;
        try {
            textLabel = new Label(new String(textBytes, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            textLabel = new Label(text);
        }

        hBox.getChildren().addAll(boldLabel, textLabel);

        return hBox;
    }
}
