package mbud.sudokusolver.controller;

import javafx.fxml.Initializable;
import mbud.sudokusolver.controller.data.ControllerData;
import mbud.sudokusolver.model.SudokuModel;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public abstract class AbstractController implements Initializable {

    private MainWindowController mainWindowController;

    protected ResourceBundle resourceBundle;
    protected ControllerData<Object> controllerData = new ControllerData<>();

    public void setMainWindowController(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resourceBundle = resources;
    }

    public final void initData(ControllerData<Object> controllerData) {
        this.controllerData = controllerData;
    }

    public void initializeAfterLoad() {

    }

    public MainWindowController getMainWindowController() {
        return mainWindowController;
    }

    public void onWindowClose() {

    }

    public SudokuModel getCurrentSudoku() {
        return null;
    }

    public String getLocalized(String key) {
        return resourceBundle.getString(key);
    }

    public String getLocalized(String key, Object... args) {
        return String.format(resourceBundle.getString(key), args);
    }

    public <E extends Enum<E>> String getLocalized(Enum<E> enumerator) {
        return getLocalized(getClassName(enumerator.getDeclaringClass()) + enumerator.name());
    }

    public <E extends Enum<E>> String getLocalized(Enum<E> enumerator, Object... args) {
        return getLocalized(getClassName(enumerator.getDeclaringClass()) + enumerator.name(), args);
    }

    private String getClassName(Class<?> classInstance) {
        List<String> classes = new ArrayList<>();
        do {
            classes.add(classInstance.getSimpleName());
            classInstance = classInstance.getDeclaringClass();
        }
        while (classInstance != null);
        StringBuilder sb = new StringBuilder();
        for (int i = classes.size() - 1; i >= 0; i--) {
            sb.append(classes.get(i));
            sb.append(".");
        }
        return sb.toString();
    }

    public <E extends Enum<E>> E getValueOfEnum(Class<E> enumClass, String value) {
        for (E e : enumClass.getEnumConstants()) {
            if (getLocalized(e).equals(value)) {
                return e;
            }
        }
        return null;
    }
}
