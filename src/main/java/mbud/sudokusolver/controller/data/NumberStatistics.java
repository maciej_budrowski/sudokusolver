package mbud.sudokusolver.controller.data;

import java.util.ArrayList;
import java.util.List;

public class NumberStatistics {
    private List<Number> numbers = new ArrayList<>();
    private Number min;
    private Number max;
    private Number avg;
    private Number med;

    public void addValue(Number value) {
        numbers.add(value);
        numbers.sort((o1, o2) -> (int) (o1.doubleValue() - o2.doubleValue()));

        min = numbers.get(0);
        max = numbers.get(numbers.size() - 1);
        if (numbers.size() % 2 == 0) {
            med = (numbers.get((numbers.size() - 1) / 2).doubleValue() + numbers.get(numbers.size() / 2).doubleValue()) / 2.0;
        } else {
            med = numbers.get(numbers.size() / 2);
        }
        avg = numbers.stream().reduce(0, (number, number2) -> number.doubleValue() + number2.doubleValue()).doubleValue() / numbers.size();
    }

    public Number getMin() {
        return min;
    }

    public Number getMax() {
        return max;
    }

    public Number getAvg() {
        return avg;
    }

    public Number getMed() {
        return med;
    }
}
