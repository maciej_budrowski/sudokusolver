package mbud.sudokusolver.controller.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class StatisticsDTO {
    private ObservableList<HeaderStatisticDTO> headerStatistics = FXCollections.observableArrayList();
    private ObservableList<StatisticDTO> statistics = FXCollections.observableArrayList();

    public ObservableList<HeaderStatisticDTO> getHeaderStatistics() {
        return headerStatistics;
    }

    public void addHeaderStatistic(HeaderStatisticDTO statistic) {
        headerStatistics.add(statistic);
    }

    public ObservableList<StatisticDTO> getStatistics() {
        return statistics;
    }

    public void addStatistic(StatisticDTO statistic) {
        statistics.add(statistic);
    }
}
