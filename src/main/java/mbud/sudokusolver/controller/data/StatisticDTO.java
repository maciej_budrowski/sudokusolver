package mbud.sudokusolver.controller.data;

import javafx.beans.property.SimpleStringProperty;

public class StatisticDTO {
    private SimpleStringProperty name;
    private SimpleStringProperty min;
    private SimpleStringProperty avg;
    private SimpleStringProperty med;
    private SimpleStringProperty max;

    public StatisticDTO() {
        name = new SimpleStringProperty();
        min = new SimpleStringProperty();
        avg = new SimpleStringProperty();
        med = new SimpleStringProperty();
        max = new SimpleStringProperty();
    }

    public StatisticDTO(String name, String min, String avg, String med, String max) {
        this();
        setName(name);
        setMin(min);
        setAvg(avg);
        setMed(med);
        setMax(max);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getMin() {
        return min.get();
    }

    public SimpleStringProperty minProperty() {
        return min;
    }

    public void setMin(String min) {
        this.min.set(min);
    }

    public String getAvg() {
        return avg.get();
    }

    public SimpleStringProperty avgProperty() {
        return avg;
    }

    public void setAvg(String avg) {
        this.avg.set(avg);
    }

    public String getMed() {
        return med.get();
    }

    public SimpleStringProperty medProperty() {
        return med;
    }

    public void setMed(String med) {
        this.med.set(med);
    }

    public String getMax() {
        return max.get();
    }

    public SimpleStringProperty maxProperty() {
        return max;
    }

    public void setMax(String max) {
        this.max.set(max);
    }
}
