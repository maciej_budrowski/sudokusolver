package mbud.sudokusolver.controller.data;

import javafx.beans.property.SimpleStringProperty;

public class HeaderStatisticDTO {
    private SimpleStringProperty name;
    private SimpleStringProperty val;

    public HeaderStatisticDTO() {
        name = new SimpleStringProperty();
        val = new SimpleStringProperty();
    }

    public HeaderStatisticDTO(String name, String val) {
        this();
        setName(name);
        setVal(val);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getVal() {
        return val.get();
    }

    public SimpleStringProperty valProperty() {
        return val;
    }

    public void setVal(String val) {
        this.val.set(val);
    }
}
