package mbud.sudokusolver.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmOptions;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmType;
import mbud.sudokusolver.csp.algorithm.heuristics.DomainSplitHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.ValueSelectHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.VarSelectHeuristic;
import mbud.sudokusolver.csp.algorithm.impl.Backtrack;
import mbud.sudokusolver.csp.algorithm.impl.DomainSplitting;
import mbud.sudokusolver.csp.algorithm.impl.GreedyDescent;
import mbud.sudokusolver.csp.algorithm.impl.SimulatedAnnealing;
import mbud.sudokusolver.threading.SudokuAlgorithmThread;
import mbud.sudokusolver.util.SpinnerUtils;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class AlgorithmOptionsController extends AbstractController {

    public static final String DATA_ALGORITHM_TYPE = "DATA_ALGORITHM_TYPE";
    public static final String DATA_BACKGROUND_WORK = "DATA_BACKGROUND_WORK";
    public static final String DATA_TIME_INTERVAL = "DATA_TIME_INTERVAL";
    public static final String DATA_STOPPING_CONDITION = "DATA_STOPPING_CONDITION";
    public static final String DATA_REPEAT_COUNT = "DATA_REPEAT_COUNT";

    private static final StringConverter<Double> doubleConverter = new StringConverter<Double>() {
        private static final double DEFAULT_VALUE = 0.9;
        private static final String DEFAULT_VALUE_STRING = "0.9";
        private DecimalFormat doubleFormat = getDecimalFormat(',');
        private DecimalFormat doubleFormatDot = getDecimalFormat('.');

        private DecimalFormat getDecimalFormat(char separator) {
            DecimalFormat decimalFormat = new DecimalFormat("0.#####");
            DecimalFormatSymbols formatSymbols = decimalFormat.getDecimalFormatSymbols();
            formatSymbols.setDecimalSeparator(separator);
            decimalFormat.setDecimalFormatSymbols(formatSymbols);
            return decimalFormat;
        }

        private Double parse(DecimalFormat format, String text) {
            try {
                return format.parse(text).doubleValue();
            } catch (ParseException e) {
                e.printStackTrace();
                return DEFAULT_VALUE;
            }
        }

        @Override
        public String toString(Double object) {
            if (object == null) {
                return DEFAULT_VALUE_STRING;
            }
            return doubleFormat.format(object);
        }

        @Override
        public Double fromString(String string) {
            if (string == null) {
                return DEFAULT_VALUE;
            }
            if (string.contains(",")) {
                return parse(doubleFormat, string);
            }
            return parse(doubleFormatDot, string);
        }
    };

    @FXML
    private GridPane algorithmParametersGridPane;

    @FXML
    private ComboBox algorithmComboBox;

    @FXML
    private GridPane commonParametersGridPane;

    @FXML
    private Button saveButton;

    @FXML
    private Button cancelButton;

    private Map<String, Object> parametersMap = new HashMap<>();

    private ObservableList<String> algorithmList;
    private ObservableList<String> splitHeurList;
    private ObservableList<String> varHeurList;
    private ObservableList<String> valHeurList;
    private ObservableList<String> minimaList;
    private ObservableList<String> selectionList;
    private ObservableList<String> stoppingConditionList;

    private Map<String, AlgorithmType> algorithmTypeStringMap = new HashMap<>();
    private Map<String, DomainSplitHeuristic.Type> splitHeuristicMap = new HashMap<>();
    private Map<String, VarSelectHeuristic.Type> varHeuristicMap = new HashMap<>();
    private Map<String, ValueSelectHeuristic.Type> valueHeuristicMap = new HashMap<>();
    private Map<String, GreedyDescent.LocalMinimumReaction> minimumReactionMap = new HashMap<>();
    private Map<String, GreedyDescent.SelectionMode> selectionModeMap = new HashMap<>();
    private Map<String, SudokuAlgorithmThread.RepeatConditions> repeatConditionsMap = new HashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        setupLocalizedValues();
    }

    @Override
    public void initializeAfterLoad() {
        setupCombobox(algorithmComboBox, DATA_ALGORITHM_TYPE, algorithmList, algorithmTypeStringMap);

        CheckBox backgroundCheckBox = new CheckBox();
        setupCheckbox(backgroundCheckBox, DATA_BACKGROUND_WORK);
        commonParametersGridPane.add(backgroundCheckBox, 1, 0);

        Spinner<Integer> repeatCountSpinner = SpinnerUtils.createSpinner(1, 1000, 50, 1);
        setupSpinnerInteger(repeatCountSpinner, DATA_TIME_INTERVAL);
        commonParametersGridPane.add(repeatCountSpinner, 1, 1);

        repeatCountSpinner = SpinnerUtils.createSpinner(1, 1000, 1, 1);
        setupSpinnerInteger(repeatCountSpinner, DATA_REPEAT_COUNT);
        commonParametersGridPane.add(repeatCountSpinner, 1, 2);

        ComboBox repeatConditionComboBox = new ComboBox();
        setupCombobox(repeatConditionComboBox, DATA_STOPPING_CONDITION, stoppingConditionList, repeatConditionsMap);
        commonParametersGridPane.add(repeatConditionComboBox, 1, 3);
    }

    private void setupLocalizedValues() {
        algorithmList = FXCollections.observableArrayList();
        for (AlgorithmType algorithmType : AlgorithmType.values()) {
            String localizedString = getLocalized(algorithmType);
            algorithmTypeStringMap.put(localizedString, algorithmType);
            algorithmList.add(localizedString);
        }

        splitHeurList = FXCollections.observableArrayList();
        for (DomainSplitHeuristic.Type domainSplitType : DomainSplitHeuristic.getTypes()) {
            String localizedString = getLocalized(domainSplitType);
            splitHeuristicMap.put(localizedString, domainSplitType);
            splitHeurList.add(localizedString);
        }

        valHeurList = FXCollections.observableArrayList();
        for (ValueSelectHeuristic.Type valHeurType : ValueSelectHeuristic.getTypes()) {
            String localizedString = getLocalized(valHeurType);
            valueHeuristicMap.put(localizedString, valHeurType);
            valHeurList.add(localizedString);
        }

        varHeurList = FXCollections.observableArrayList();
        for (VarSelectHeuristic.Type varHeurType : VarSelectHeuristic.getTypes()) {
            String localizedString = getLocalized(varHeurType);
            varHeuristicMap.put(localizedString, varHeurType);
            varHeurList.add(localizedString);
        }

        minimaList = FXCollections.observableArrayList();
        for (GreedyDescent.LocalMinimumReaction reaction : GreedyDescent.getReactions()) {
            String localizedString = getLocalized(reaction);
            minimumReactionMap.put(localizedString, reaction);
            minimaList.add(localizedString);
        }

        selectionList = FXCollections.observableArrayList();
        for (GreedyDescent.SelectionMode selectionMode : GreedyDescent.getSelectionModes()) {
            String localizedString = getLocalized(selectionMode);
            selectionModeMap.put(localizedString, selectionMode);
            selectionList.add(localizedString);
        }

        stoppingConditionList = FXCollections.observableArrayList();
        for (SudokuAlgorithmThread.RepeatConditions repeatCondition : SudokuAlgorithmThread.RepeatConditions.values()) {
            String localizedString = getLocalized(repeatCondition);
            repeatConditionsMap.put(localizedString, repeatCondition);
            stoppingConditionList.add(localizedString);
        }
    }

    private void setupGridPane(AlgorithmType algorithmType) {
        List<AlgorithmOptions> algorithmOptions;
        switch (algorithmType) {
            case BACKTRACK:
                algorithmOptions = Backtrack.getAvailableOptions();
                break;
            case DOMAIN_SPLITTING:
                algorithmOptions = DomainSplitting.getAvailableOptions();
                break;
            case GREEDY_DESCENT:
                algorithmOptions = GreedyDescent.getAvailableOptions();
                break;
            case SIMULATED_ANNEALING:
                algorithmOptions = SimulatedAnnealing.getAvailableOptions();
                break;
            default:
                return;
        }
        algorithmParametersGridPane.getRowConstraints().clear();
        algorithmParametersGridPane.getChildren().clear();
        for (int i = 0; i < algorithmOptions.size(); i++) {
            algorithmParametersGridPane.getRowConstraints().add(getRowConstraints());
            setupRow(i, algorithmOptions.get(i));
        }
    }

    public void setupRow(int index, AlgorithmOptions algorithmOptions) {
        switch (algorithmOptions) {
            case INITIAL_GAC:
            case FORWARD_CHECKING:
                CheckBox checkBox = new CheckBox();
                setupCheckbox(checkBox, algorithmOptions.toString());
                algorithmParametersGridPane.addRow(index, new Label(getLocalized(algorithmOptions)), checkBox);
                break;
            case VARIABLE_HEURISTIC:
            case VALUE_HEURISTIC:
            case SPLIT_HEURISTIC:
            case LOCAL_MINIMUM_REACTION:
            case SELECTION_MODE:
                ComboBox comboBox = new ComboBox();
                if (algorithmOptions == AlgorithmOptions.VARIABLE_HEURISTIC) {
                    setupCombobox(comboBox, algorithmOptions.toString(), varHeurList, varHeuristicMap);
                } else if (algorithmOptions == AlgorithmOptions.VALUE_HEURISTIC) {
                    setupCombobox(comboBox, algorithmOptions.toString(), valHeurList, valueHeuristicMap);
                } else if (algorithmOptions == AlgorithmOptions.SPLIT_HEURISTIC) {
                    setupCombobox(comboBox, algorithmOptions.toString(), splitHeurList, splitHeuristicMap);
                } else if (algorithmOptions == AlgorithmOptions.LOCAL_MINIMUM_REACTION) {
                    setupCombobox(comboBox, algorithmOptions.toString(), minimaList, minimumReactionMap);
                } else {
                    setupCombobox(comboBox, algorithmOptions.toString(), selectionList, selectionModeMap);
                }
                algorithmParametersGridPane.addRow(index, new Label(getLocalized(algorithmOptions)), comboBox);
                break;
            case MAX_STEPS:
            case NUMBER_OF_SPLITS:
                Spinner<Integer> spinnerInteger;
                if (algorithmOptions == AlgorithmOptions.MAX_STEPS) {
                    spinnerInteger = SpinnerUtils.createSpinner(1, 100000000, 1000, 1);
                } else {
                    spinnerInteger = SpinnerUtils.createSpinner(2, 100, 2, 1);
                }
                setupSpinnerInteger(spinnerInteger, algorithmOptions.toString());
                algorithmParametersGridPane.addRow(index, new Label(getLocalized(algorithmOptions)), spinnerInteger);
                break;
            case STARTING_TEMPERATURE:
            case TEMPERATURE_MULTIPLIER:
                Spinner<Double> spinnerDouble;
                if (algorithmOptions == AlgorithmOptions.STARTING_TEMPERATURE) {
                    spinnerDouble = SpinnerUtils.createSpinner(0.001, 1000000.0, 1.0, 0.1);
                } else {
                    spinnerDouble = SpinnerUtils.createSpinner(0.001, 0.99999, 0.99, 0.0001);
                }
                setupSpinnerDouble(spinnerDouble, algorithmOptions.toString());
                algorithmParametersGridPane.addRow(index, new Label(getLocalized(algorithmOptions)), spinnerDouble);
                break;
        }
    }

    @SuppressWarnings("unchecked")
    private <T extends Enum<T>> void setupCombobox(ComboBox comboBox, String paramName, ObservableList<String> paramList, Map<String, T> stringObjectMap) {
        comboBox.setItems(paramList);
        comboBox.valueProperty().addListener((o, oldVal, newVal) -> {
            parametersMap.put(paramName, stringObjectMap.get(newVal.toString()));
            if (comboBox == algorithmComboBox) {
                setupGridPane((AlgorithmType) stringObjectMap.get(newVal.toString()));
            }
        });
        if (controllerData.getParameter(paramName) != null) {
            comboBox.setValue(getLocalized((T) controllerData.getParameter(paramName)));
        } else {
            comboBox.setValue(paramList.get(0));
        }
    }

    private void setupCheckbox(CheckBox checkBox, String paramName) {
        checkBox.selectedProperty().addListener((o, oldVal, newVal) -> controllerData.setParameter(paramName, newVal));
        if (controllerData.getParameter(paramName) != null) {
            checkBox.setSelected((Boolean) controllerData.getParameter(paramName));
        } else {
            checkBox.setSelected(false);
        }
    }

    private void setupSpinnerInteger(Spinner<Integer> spinner, String paramName) {
        spinner.valueProperty().addListener((o, oldVal, newVal) -> parametersMap.put(paramName, newVal));
        if (controllerData.getParameter(paramName) != null) {
            spinner.getValueFactory().setValue(Integer.parseInt(controllerData.getParameter(paramName).toString()));
        } else {
            spinner.getValueFactory().setValue(spinner.getValue());
        }
    }

    private void setupSpinnerDouble(Spinner<Double> spinner, String paramName) {
        spinner.valueProperty().addListener((o, oldVal, newVal) -> parametersMap.put(paramName, newVal));
        spinner.getValueFactory().setConverter(doubleConverter);
        if (controllerData.getParameter(paramName) != null) {
            spinner.getValueFactory().setValue(doubleConverter.fromString(controllerData.getParameter(paramName).toString()));
        } else {
            spinner.getValueFactory().setValue(spinner.getValue());
        }
    }

    private RowConstraints getRowConstraints() {
        return new RowConstraints(30, 30, -1);
    }

    public void saveButtonPressed() {
        controllerData.putData(parametersMap);
        Stage stage = (Stage) saveButton.getScene().getWindow();
        stage.close();
    }

    public void cancelButtonPressed() {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }
}
