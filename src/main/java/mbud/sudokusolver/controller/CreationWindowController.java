package mbud.sudokusolver.controller;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import mbud.sudokusolver.control.CanvasHolder;
import mbud.sudokusolver.control.SudokuCanvas;
import mbud.sudokusolver.csp.SudokuCSP;
import mbud.sudokusolver.model.*;
import mbud.sudokusolver.util.Pair;
import mbud.sudokusolver.util.SpinnerUtils;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class CreationWindowController extends AbstractController {
    public static final String PARAM_SUDOKU_TYPE = "PARAM_SUDOKU_TYPE";
    public static final String PARAM_SUDOKU_WIDTH = "PARAM_SUDOKU_WIDTH";
    public static final String PARAM_SUDOKU_HEIGHT = "PARAM_SUDOKU_HEIGHT";

    private enum Mode {
        SET_VALUE, CREATE_GROUP, SELECT_GROUP, ADD_TO_GROUP
    }

    @FXML
    public Button setValueButton;

    @FXML
    public Button createGroupButton;

    @FXML
    public Button selectGroupButton;

    @FXML
    public Button addToGroupButton;

    @FXML
    public Button editLimitButton;

    @FXML
    public Button removeGroupButton;

    @FXML
    public Button simulationButton;

    @FXML
    private AnchorPane centerPane;

    private SudokuModel sudokuModel;
    private SudokuType sudokuType;
    private SudokuCanvas canvas;

    private Mode currentMode = Mode.SET_VALUE;
    private Pair<List<Pair<Integer, Integer>>, Integer> currentGroup;

    @Override
    public void initializeAfterLoad() {

        if (sudokuModel == null) {
            initializeSudokuFromParams();
        }
        setupButtonVisibility();

        canvas = SudokuCanvas.createFrom(sudokuModel, 50, 30);

        CanvasHolder canvasHolder = new CanvasHolder(canvas);
        centerPane.getChildren().add(canvasHolder);
        AnchorPane.setBottomAnchor(canvasHolder, 0.0);
        AnchorPane.setTopAnchor(canvasHolder, 0.0);
        AnchorPane.setLeftAnchor(canvasHolder, 0.0);
        AnchorPane.setRightAnchor(canvasHolder, 0.0);

        redrawCanvas();
        canvas.registerClickListener((x, y) -> {
            switch (currentMode) {
                case SET_VALUE:
                    Dialog<Integer> dialog = createSetValueDialog(x, y);
                    Optional<Integer> result = dialog.showAndWait();
                    result.ifPresent(v -> {
                        sudokuModel.setValue(x, y, v);
                        redrawCanvas();
                    });
                    break;
                case CREATE_GROUP:
                    List<Pair<List<Pair<Integer, Integer>>, Integer>> groups = ((KillerSudokuModel) sudokuModel).getGroups();
                    Pair<Integer, Integer> coord = new Pair<>(x, y);
                    boolean hasGroup = false;
                    for (Pair<List<Pair<Integer, Integer>>, Integer> group : groups) {
                        if (group.getA().contains(coord)) {
                            hasGroup = true;
                            break;
                        }
                    }
                    if (!hasGroup) {
                        dialog = createCreateGroupDialog();
                        result = dialog.showAndWait();
                        result.ifPresent(v -> {
                            currentGroup = ((KillerSudokuModel) sudokuModel).addGroup(v, x, y);
                            currentMode = Mode.SELECT_GROUP;
                            setupButtonVisibility();
                            redrawCanvas();
                        });
                    }
                    break;
                case SELECT_GROUP:
                    groups = ((KillerSudokuModel) sudokuModel).getGroups();
                    coord = new Pair<>(x, y);
                    for (Pair<List<Pair<Integer, Integer>>, Integer> group : groups) {
                        if (group.getA().contains(coord)) {
                            currentGroup = group;
                            setupButtonVisibility();
                            break;
                        }
                    }
                    break;
                case ADD_TO_GROUP:
                    groups = ((KillerSudokuModel) sudokuModel).getGroups();
                    coord = new Pair<>(x, y);
                    hasGroup = false;
                    for (Pair<List<Pair<Integer, Integer>>, Integer> group : groups) {
                        if (group.getA().contains(coord)) {
                            hasGroup = true;
                            break;
                        }
                    }
                    if (!hasGroup) {
                        currentGroup.getA().add(new Pair<>(x, y));
                        ((KillerSudokuModel) sudokuModel).sortGroupMembers();
                        redrawCanvas();
                    }
                    break;
            }
        });
        canvas.registerRightClickListener(this::handleRightClick);
    }

    private Dialog<Integer> createSetValueDialog(int x, int y) {
        Dialog<Integer> dialog = new Dialog<>();
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        dialog.setTitle(getLocalized("creationWindow.setValueDialog"));
        dialog.setHeaderText(getLocalized("creationWindow.setValueOfField"));
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        Label label = new Label(getLocalized("creationWindow.value"));
        Spinner<Integer> spinner = SpinnerUtils.createSpinner(1, sudokuModel.getMaxValue(), sudokuModel.getValueAt(x, y) == 0 ? 1 : sudokuModel.getValueAt(x, y), 1);
        grid.add(label, 0, 0);
        grid.add(spinner, 1, 0);
        dialog.getDialogPane().setContent(grid);
        dialog.setResultConverter(param -> {
            if (param == ButtonType.OK) {
                return spinner.getValue();
            }
            return null;
        });
        return dialog;
    }

    private Dialog<Integer> createCreateGroupDialog() {
        Dialog<Integer> dialog;
        GridPane grid;
        Label label;
        Spinner<Integer> spinner;
        dialog = new Dialog<>();
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        dialog.setTitle(getLocalized("creationWindow.setLimit"));
        dialog.setHeaderText(getLocalized("creationWindow.setLimitGroup"));
        grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        label = new Label(getLocalized("creationWindow.limit"));
        spinner = new Spinner<>(1, 315, 15, 1);
        spinner.setEditable(true);
        grid.add(label, 0, 0);
        grid.add(spinner, 1, 0);
        dialog.getDialogPane().setContent(grid);
        dialog.setResultConverter(param -> {
            if (param == ButtonType.OK) {
                return spinner.getValue();
            }
            return null;
        });
        return dialog;
    }

    private void handleRightClick(int x, int y) {
        if (Mode.SET_VALUE.equals(currentMode)) {
            sudokuModel.setValue(x, y, 0);
            redrawCanvas();
        } else if (Mode.ADD_TO_GROUP.equals(currentMode)) {
            currentGroup.getA().remove(new Pair<>(x, y));
            if (currentGroup.getA().isEmpty()) {
                ((KillerSudokuModel) sudokuModel).getGroups().remove(currentGroup);
                currentGroup = null;
                setupButtonVisibility();
            } else {
                ((KillerSudokuModel) sudokuModel).sortGroupMembers();
            }
            redrawCanvas();
        }
    }

    private void initializeSudokuFromParams() {
        sudokuType = (SudokuType) controllerData.getParameter(PARAM_SUDOKU_TYPE);
        Integer width = (Integer) controllerData.getParameter(PARAM_SUDOKU_WIDTH);
        Integer height = (Integer) controllerData.getParameter(PARAM_SUDOKU_HEIGHT);
        switch (sudokuType) {
            case SUDOKU:
                sudokuModel = new BasicSudokuModel(width, height);
                break;
            case KILLER_SUDOKU:
                sudokuModel = new KillerSudokuModel();
                break;
            case SAMURAI_SUDOKU:
                sudokuModel = new SamuraiSudokuModel();
                break;
        }
    }

    protected void initializeSudoku(SudokuModel model) {
        sudokuModel = model;
        sudokuType = model.getType();
    }

    private void redrawCanvas() {
        SudokuCSP csp = sudokuModel.toCSP();
        canvas.setCsp(csp);
        canvas.setAssignment(csp.startingAssignment());
        canvas.setupBoard();
        canvas.drawVariables(csp.getVariables(), true);
    }

    private void setupButtonVisibility() {
        setValueButton.setVisible(true);
        setValueButton.setDisable(currentMode == Mode.SET_VALUE);
        simulationButton.setVisible(true);
        simulationButton.setDisable(false);
        if (sudokuType == SudokuType.KILLER_SUDOKU) {
            createGroupButton.setVisible(true);
            createGroupButton.setDisable(currentMode == Mode.CREATE_GROUP);
            selectGroupButton.setVisible(true);
            selectGroupButton.setDisable(currentMode == Mode.SELECT_GROUP);
            addToGroupButton.setVisible(true);
            addToGroupButton.setDisable(currentMode == Mode.ADD_TO_GROUP);
            editLimitButton.setVisible(true);
            editLimitButton.setDisable(currentGroup == null);
            removeGroupButton.setVisible(true);
            removeGroupButton.setDisable(currentGroup == null);
        } else {
            createGroupButton.setVisible(false);
            selectGroupButton.setVisible(false);
            addToGroupButton.setVisible(false);
            editLimitButton.setVisible(false);
            removeGroupButton.setVisible(false);
        }
    }

    public void setValuePressed() {
        currentMode = Mode.SET_VALUE;

        setupButtonVisibility();
    }

    public void createGroupPressed() {
        currentMode = Mode.CREATE_GROUP;

        setupButtonVisibility();
    }

    public void selectGroupPressed() {
        currentMode = Mode.SELECT_GROUP;

        setupButtonVisibility();
    }

    public void addToGroupPressed() {
        currentMode = Mode.ADD_TO_GROUP;

        setupButtonVisibility();
    }

    public void editLimitPressed() {
        if (currentGroup == null) {
            return;
        }

        Dialog<Integer> dialog = new Dialog<>();
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        dialog.setTitle(getLocalized("creationWindow.setLimit"));
        dialog.setHeaderText(getLocalized("creationWindow.setLimitGroup"));
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        Label label = new Label(getLocalized("creationWindow.limit"));
        Spinner<Integer> spinner = SpinnerUtils.createSpinner(1, 45, currentGroup.getB(), 1);
        grid.add(label, 0, 0);
        grid.add(spinner, 1, 0);
        dialog.getDialogPane().setContent(grid);
        dialog.setResultConverter(param -> {
            if (param == ButtonType.OK) {
                return spinner.getValue();
            }
            return null;
        });
        Optional<Integer> result = dialog.showAndWait();
        result.ifPresent(v -> {
            currentGroup.setB(v);
            redrawCanvas();
        });
    }

    public void removeGroupPressed() {
        ((KillerSudokuModel) sudokuModel).getGroups().remove(currentGroup);
        currentGroup = null;
        currentMode = Mode.SELECT_GROUP;
        setupButtonVisibility();
        redrawCanvas();
    }

    public void simulationPressed() throws IOException {

        getMainWindowController().
                loadContentPane("layout/simulation_window.fxml", c -> {
                    SimulationWindowController controller = (SimulationWindowController) c;
                    controller.setSudoku(sudokuModel);
                });
    }

    @Override
    public SudokuModel getCurrentSudoku() {
        return sudokuModel;
    }
}
