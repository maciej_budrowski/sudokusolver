package mbud.sudokusolver.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import mbud.sudokusolver.control.CanvasHolder;
import mbud.sudokusolver.control.MessageLog;
import mbud.sudokusolver.control.SudokuCanvas;
import mbud.sudokusolver.controller.data.HeaderStatisticDTO;
import mbud.sudokusolver.controller.data.NumberStatistics;
import mbud.sudokusolver.controller.data.StatisticDTO;
import mbud.sudokusolver.controller.data.StatisticsDTO;
import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.SudokuCSP;
import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.algorithm.SolvingAlgorithm;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmOptions;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmStatistic;
import mbud.sudokusolver.csp.algorithm.enums.AlgorithmType;
import mbud.sudokusolver.csp.algorithm.event.AlgorithmDomainChanged;
import mbud.sudokusolver.csp.algorithm.event.AlgorithmEndListener;
import mbud.sudokusolver.csp.algorithm.event.AlgorithmMessageListener;
import mbud.sudokusolver.csp.algorithm.heuristics.DomainSplitHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.ValueSelectHeuristic;
import mbud.sudokusolver.csp.algorithm.heuristics.VarSelectHeuristic;
import mbud.sudokusolver.csp.algorithm.impl.Backtrack;
import mbud.sudokusolver.csp.algorithm.impl.DomainSplitting;
import mbud.sudokusolver.csp.algorithm.impl.GreedyDescent;
import mbud.sudokusolver.csp.algorithm.impl.SimulatedAnnealing;
import mbud.sudokusolver.model.SudokuModel;
import mbud.sudokusolver.threading.SudokuAlgorithmThread;
import mbud.sudokusolver.util.Pair;
import mbud.sudokusolver.util.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class SimulationWindowController extends AbstractController implements AlgorithmMessageListener, AlgorithmDomainChanged<Integer>, AlgorithmEndListener<Integer> {

    @FXML
    private AnchorPane logPane;

    private MessageLog messageLog;

    @FXML
    private AnchorPane centerPane;

    @FXML
    private VBox statisticsView;

    @FXML
    private Button startButton;

    @FXML
    private Button resetButton;

    @FXML
    private Button stepButton;

    @FXML
    private Button pauseButton;

    @FXML
    private Button optionsButton;

    @FXML
    public Button editButton;

    private SudokuAlgorithmThread thread;
    private Map<AlgorithmStatistic, Label> statisticToLabelMap = new EnumMap<>(AlgorithmStatistic.class);

    private SudokuCanvas canvas;
    private SudokuCSP csp;
    private SudokuModel sudokuModel;
    private SolvingAlgorithm<Integer> solvingAlgorithm;

    private List<Map<AlgorithmStatistic, Number>> statistics = new ArrayList<>();
    private int finishedAlgorithms;
    private int successfulAlgorithms;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        startButton.setDisable(true);
        pauseButton.setDisable(true);
        stepButton.setDisable(true);
        resetButton.setDisable(true);
        optionsButton.setDisable(true);
        editButton.setDisable(true);

        messageLog = new MessageLog(500, 100);
        AnchorPane.setTopAnchor(messageLog, 0.0);
        AnchorPane.setRightAnchor(messageLog, 0.0);
        AnchorPane.setBottomAnchor(messageLog, 0.0);
        AnchorPane.setLeftAnchor(messageLog, 0.0);
        logPane.getChildren().add(messageLog);
    }

    public void setSudoku(SudokuModel sudoku) {
        this.sudokuModel = sudoku;
        csp = sudoku.toCSP();
        canvas = SudokuCanvas.createFrom(sudoku, 50, 30);
        canvas.setupBoard();

        CanvasHolder canvasHolder = new CanvasHolder(canvas);
        centerPane.getChildren().add(canvasHolder);
        AnchorPane.setBottomAnchor(canvasHolder, 0.0);
        AnchorPane.setTopAnchor(canvasHolder, 0.0);
        AnchorPane.setLeftAnchor(canvasHolder, 0.0);
        AnchorPane.setRightAnchor(canvasHolder, 0.0);

        canvas.setCsp(csp);
        canvas.setAssignment(csp.startingAssignment());
        canvas.drawVariables(csp.getVariables(), true);

        startButton.setDisable(false);
        stepButton.setDisable(false);
        optionsButton.setDisable(false);
        editButton.setDisable(false);
    }

    private void setupAlgorithm() {
        AlgorithmType algorithmType = (AlgorithmType) controllerData.getParameter(AlgorithmOptionsController.DATA_ALGORITHM_TYPE);
        if (algorithmType == null) {
            algorithmType = AlgorithmType.BACKTRACK;
        }
        Boolean isDoingGAC = (Boolean) controllerData.getParameter(AlgorithmOptions.INITIAL_GAC.toString());
        if (isDoingGAC == null) {
            isDoingGAC = false;
        }
        switch (algorithmType) {
            case BACKTRACK:
                setupBacktrackAlgorithm(isDoingGAC);
                break;
            case DOMAIN_SPLITTING:
                setupDomainSplitting();
                break;
            case GREEDY_DESCENT:
                setupGreedyDescent(isDoingGAC);
                break;
            case SIMULATED_ANNEALING:
                setupSimulatedAnnealing(isDoingGAC);
                break;
        }

        solvingAlgorithm.registerDomainChangedListener(this);
        solvingAlgorithm.registerMessageListener(this);

        Integer numberOfRepeats = (Integer) controllerData.getParameter(AlgorithmOptionsController.DATA_REPEAT_COUNT);
        if (numberOfRepeats == null) {
            numberOfRepeats = 1;
        }
        Integer timeInterval = (Integer) controllerData.getParameter(AlgorithmOptionsController.DATA_TIME_INTERVAL);
        if (timeInterval == null) {
            timeInterval = 50;
        }
        Boolean backgroundWork = (Boolean) controllerData.getParameter(AlgorithmOptionsController.DATA_BACKGROUND_WORK);
        if (backgroundWork == null) {
            backgroundWork = false;
        }
        SudokuAlgorithmThread.RepeatConditions repeatConditions = (SudokuAlgorithmThread.RepeatConditions) controllerData.getParameter(AlgorithmOptionsController.DATA_STOPPING_CONDITION);
        if (repeatConditions == null) {
            repeatConditions = SudokuAlgorithmThread.RepeatConditions.REPEAT_UNTIL_SOLUTION;
        }

        thread = new SudokuAlgorithmThread(solvingAlgorithm);
        thread.setNoPause(backgroundWork);
        thread.setInterval(timeInterval);
        thread.setRepeats(numberOfRepeats);
        thread.setRepeatCondition(repeatConditions);
        thread.registerAlgorithmEndListener(this);

        statisticsView.getChildren().clear();
        List<AlgorithmStatistic> algorithmStatistics = new ArrayList<>(solvingAlgorithm.getAvailableStatistics());
        algorithmStatistics.add(0, AlgorithmStatistic.CUR_RESTART);
        for (AlgorithmStatistic statistic : algorithmStatistics) {
            Label label = new Label();
            if (statistic == AlgorithmStatistic.CUR_RESTART) {
                label.setText(getLocalized(statistic) + ": " + "0/" + thread.getRepeats());
            } else {
                label.setText(getLocalized(statistic) + ": " + solvingAlgorithm.getStatistics().get(statistic));
            }
            statisticToLabelMap.put(statistic, label);
            statisticsView.getChildren().add(label);
        }
    }

    private void setupBacktrackAlgorithm(Boolean isDoingGAC) {
        Boolean isForwardChecking = (Boolean) controllerData.getParameter(AlgorithmOptions.FORWARD_CHECKING.toString());
        if (isForwardChecking == null) {
            isForwardChecking = false;
        }
        VarSelectHeuristic.Type varHeurType = (VarSelectHeuristic.Type) controllerData.getParameter(AlgorithmOptions.VARIABLE_HEURISTIC.toString());
        if (varHeurType == null) {
            varHeurType = VarSelectHeuristic.Type.FIRST_VARIABLE;
        }
        ValueSelectHeuristic.Type valHeurType = (ValueSelectHeuristic.Type) controllerData.getParameter(AlgorithmOptions.VALUE_HEURISTIC.toString());
        if (valHeurType == null) {
            valHeurType = ValueSelectHeuristic.Type.FIRST_VALUE;
        }
        solvingAlgorithm = new Backtrack<>(csp, isDoingGAC, isForwardChecking, varHeurType, valHeurType);
    }

    private void setupDomainSplitting() {
        VarSelectHeuristic.Type varHeurType;
        varHeurType = (VarSelectHeuristic.Type) controllerData.getParameter(AlgorithmOptions.VARIABLE_HEURISTIC.toString());
        if (varHeurType == null) {
            varHeurType = VarSelectHeuristic.Type.FIRST_VARIABLE;
        }
        DomainSplitHeuristic.Type splitHeurType = (DomainSplitHeuristic.Type) controllerData.getParameter(AlgorithmOptions.SPLIT_HEURISTIC.toString());
        if (splitHeurType == null) {
            splitHeurType = DomainSplitHeuristic.Type.BASIC_SPLIT;
        }
        Integer numberOfCuts = (Integer) controllerData.getParameter(AlgorithmOptions.NUMBER_OF_SPLITS.toString());
        if (numberOfCuts == null) {
            numberOfCuts = 2;
        }
        solvingAlgorithm = new DomainSplitting<>(csp, varHeurType, splitHeurType, numberOfCuts);
    }

    private void setupGreedyDescent(Boolean isDoingGAC) {
        Integer maxSteps = (Integer) controllerData.getParameter(AlgorithmOptions.MAX_STEPS.toString());
        if (maxSteps == null) {
            maxSteps = 1000;
        }
        GreedyDescent.LocalMinimumReaction minimumReaction = (GreedyDescent.LocalMinimumReaction) controllerData.getParameter(AlgorithmOptions.LOCAL_MINIMUM_REACTION.toString());
        if (minimumReaction == null) {
            minimumReaction = GreedyDescent.LocalMinimumReaction.RANDOM_RESTART;
        }
        GreedyDescent.SelectionMode selectionMode = (GreedyDescent.SelectionMode) controllerData.getParameter(AlgorithmOptions.SELECTION_MODE.toString());
        if (selectionMode == null) {
            selectionMode = GreedyDescent.SelectionMode.BEST_PAIR;
        }
        solvingAlgorithm = new GreedyDescent<>(csp, maxSteps, minimumReaction, selectionMode, isDoingGAC);
    }

    private void setupSimulatedAnnealing(Boolean isDoingGAC) {
        Double startTemp = (Double) controllerData.getParameter(AlgorithmOptions.STARTING_TEMPERATURE.toString());
        if (startTemp == null) {
            startTemp = 1.0;
        }
        Double tempMult = (Double) controllerData.getParameter(AlgorithmOptions.TEMPERATURE_MULTIPLIER.toString());
        if (tempMult == null) {
            tempMult = 0.99;
        }
        solvingAlgorithm = new SimulatedAnnealing<>(csp, isDoingGAC, startTemp, tempMult);
    }

    public void startButtonPressed() {
        if (thread == null) {
            setupAlgorithm();
            thread.start();
        } else {
            thread.resume();
        }

        startButton.setDisable(true);
        pauseButton.setDisable(false);
        stepButton.setDisable(false);
        resetButton.setDisable(false);
        optionsButton.setDisable(true);
        editButton.setDisable(true);
    }

    public void stepButtonPressed() {
        if (thread == null) {
            setupAlgorithm();
        }

        thread.step();
        startButton.setDisable(false);
        pauseButton.setDisable(true);
        stepButton.setDisable(false);
        optionsButton.setDisable(true);
        editButton.setDisable(true);
    }

    public void pauseButtonPressed() {
        thread.pause();
        startButton.setDisable(false);
        pauseButton.setDisable(true);
        stepButton.setDisable(false);
        optionsButton.setDisable(true);
        editButton.setDisable(true);
    }

    public void resetButtonPressed() {
        if (thread != null) {
            if (thread.isRunning()) {
                thread.stop();
            }
            thread.clear();
            thread = null;
        }
        canvas.setCsp(csp);
        canvas.setAssignment(csp.startingAssignment());
        canvas.setupBoard();
        canvas.drawVariables(csp.getVariables(), true);
        statisticsView.getChildren().clear();
        messageLog.clear();
        statistics.clear();
        successfulAlgorithms = finishedAlgorithms = 0;

        startButton.setDisable(false);
        pauseButton.setDisable(true);
        stepButton.setDisable(false);
        optionsButton.setDisable(false);
        editButton.setDisable(false);
    }

    public void optionsButtonsPressed() {
        getMainWindowController().openWindow(getLocalized("algorithmOptions.title"), "layout/algorithm_options_window.fxml");
    }

    private void updateStatistics(Map<AlgorithmStatistic, Number> curStats) {
        for (Map.Entry<AlgorithmStatistic, Number> statisticObjectEntry : curStats.entrySet()) {
            Label label = statisticToLabelMap.get(statisticObjectEntry.getKey());
            if (label != null) {
                if (statisticObjectEntry.getKey().equals(AlgorithmStatistic.CUR_RESTART)) {
                    label.setText(getLocalized(statisticObjectEntry.getKey()) + ": " + StringUtils.formatNumber(statisticObjectEntry.getValue().intValue() + 1, statisticObjectEntry.getKey().getPrecision()) + "/" + StringUtils.formatNumber(curStats.get(AlgorithmStatistic.TOTAL_RESTARTS), statisticObjectEntry.getKey().getPrecision()));
                } else {
                    label.setText(getLocalized(statisticObjectEntry.getKey()) + ": " + StringUtils.formatNumber(statisticObjectEntry.getValue(), statisticObjectEntry.getKey().getPrecision()));
                }
            }
        }
    }

    @Override
    public void onMessageReceived(Enum<?> messageType, Object... arguments) {
        Platform.runLater(() -> {
            for (int i = 0; i < arguments.length; i++) {
                if (arguments[i] instanceof Variable<?>) {
                    Pair<Integer, Integer> coord = csp.coordOfVar((Variable<Integer>) arguments[i]);
                    arguments[i] = coord.toString();
                }
            }
            messageLog.addMessage(getLocalized(messageType, arguments));
        });
    }

    @Override
    public void onDomainChanged(Assignment<Integer> assignment, List<Variable<Integer>> variables) {
        Platform.runLater(() -> {
            if (thread == null) {
                return;
            }

            canvas.setAssignment(assignment);
            canvas.drawVariables(csp.getChangeableVariables().stream().filter(variables::contains).collect(Collectors.toList()), false);
            updateStatistics(thread.getStatistics());
        });
    }

    @Override
    public void onAlgorithmEnded(Assignment<Integer> finalAssignment, Map<AlgorithmStatistic, Number> curStats) {
        Platform.runLater(() -> {
            if (thread == null) {
                return;
            }

            messageLog.addMessage(getLocalized("simulationWindow.algorithmEndedAfter", curStats.get(AlgorithmStatistic.TIME_PASSED)));
            finishedAlgorithms++;
            if (finalAssignment == null) {
                messageLog.addMessage(getLocalized("simulationWindow.solutionNotFound"));
            } else {
                successfulAlgorithms++;
                messageLog.addMessage(getLocalized("simulationWindow.solutionFound"));
            }
            curStats.put(AlgorithmStatistic.SUCCESS, (finalAssignment != null) ? 1 : 0);
            statistics.add(curStats);

            canvas.setAssignment((finalAssignment != null) ? finalAssignment : solvingAlgorithm.getLastAssignment());
            canvas.drawVariables();
            updateStatistics(curStats);

            if (!thread.startRepeatIfNeeded()) {
                controllerData.setParameter(StatisticsWindowController.STATISTICS_DTO, createStatisticsDTO());

                getMainWindowController().openWindow(getLocalized("statisticsWindow.title"), "layout/statistics_window.fxml");
            }
        });
    }

    protected StatisticsDTO createStatisticsDTO() {
        StatisticsDTO statisticsDTO = new StatisticsDTO();

        Map<AlgorithmStatistic, NumberStatistics> statisticsMap = new EnumMap<>(AlgorithmStatistic.class);
        for (Map<AlgorithmStatistic, Number> stats : statistics) {
            for (Map.Entry<AlgorithmStatistic, Number> entry : stats.entrySet()) {
                AlgorithmStatistic stat = entry.getKey();
                Number val = entry.getValue();
                if (stat.isTemporal()) {
                    continue;
                }
                NumberStatistics numberStatistics;
                if (statisticsMap.containsKey(stat)) {
                    numberStatistics = statisticsMap.get(stat);
                } else {
                    numberStatistics = new NumberStatistics();
                    statisticsMap.put(stat, numberStatistics);
                }
                numberStatistics.addValue(val);
            }
        }

        for (Map.Entry<AlgorithmStatistic, NumberStatistics> statisticsEntry : statisticsMap.entrySet()) {
            StatisticDTO dto = new StatisticDTO();
            dto.setName(getLocalized(statisticsEntry.getKey()));
            dto.setMin(StringUtils.formatNumber(statisticsEntry.getValue().getMin(), statisticsEntry.getKey().getPrecision()));
            dto.setAvg(StringUtils.formatNumber(statisticsEntry.getValue().getAvg(), statisticsEntry.getKey().getPrecision()));
            dto.setMed(StringUtils.formatNumber(statisticsEntry.getValue().getMed(), statisticsEntry.getKey().getPrecision()));
            dto.setMax(StringUtils.formatNumber(statisticsEntry.getValue().getMax(), statisticsEntry.getKey().getPrecision()));
            statisticsDTO.addStatistic(dto);
        }

        double percentSuccessful = (100.0 * successfulAlgorithms) / finishedAlgorithms;
        HeaderStatisticDTO headerStatisticDTO = new HeaderStatisticDTO(getLocalized("statisticsWindow.percentSuccessful"), String.format("%.1f%% (%d/%d)", percentSuccessful, successfulAlgorithms, finishedAlgorithms));
        statisticsDTO.addHeaderStatistic(headerStatisticDTO);
        return statisticsDTO;
    }

    @Override
    public void onWindowClose() {
        if (thread != null) {
            try {
                thread.stop();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public SudokuModel getCurrentSudoku() {
        return sudokuModel;
    }

    public void editButtonPressed() throws IOException {
        getMainWindowController().
                loadContentPane("layout/creation_window.fxml", c -> {
                    CreationWindowController controller = (CreationWindowController) c;
                    controller.initializeSudoku(sudokuModel);
                });
    }
}
