package mbud.sudokusolver.csp.test;

import mbud.sudokusolver.csp.Variable;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class VariableTest {

    private Variable<Integer> variable, variableWithSetDomain;

    @Before
    public void setUp() {
        variable = new Variable<>();
        variableWithSetDomain = new Variable<>(Arrays.asList(1, 2, 3));
    }

    @Test
    public void getDomainTest() {
        assertTrue(variable.getDomain().isEmpty());
        assertFalse(variableWithSetDomain.getDomain().isEmpty());
        assertEquals(3, variableWithSetDomain.getDomain().size());
    }

    @Test
    public void restrictTest() {
        variable.restrictTo(3);
        assertEquals(1, variable.getDomain().size());
        assertEquals(3, (long) variable.getDomain().get(0));

        variable.restrictTo(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
        assertEquals(8, variable.getDomain().size());
    }

}
