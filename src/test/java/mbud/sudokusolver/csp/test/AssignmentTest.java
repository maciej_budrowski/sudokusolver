package mbud.sudokusolver.csp.test;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.Variable;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class AssignmentTest {

    private Assignment<Integer> assignment;
    private Variable<Integer> exampleVar, exampleVar2;

    @Before
    public void setUp() {
        assignment = new Assignment<>();
        exampleVar = new Variable<>(Arrays.asList(1, 2, 3));
        exampleVar2 = new Variable<>(Arrays.asList(1, 2));
        assignment.restrictDomainTo(exampleVar, exampleVar.getDomain());
        assignment.restrictDomainTo(exampleVar2, exampleVar2.getDomain());
    }

    @Test
    public void testGetDomains() {
        assertEquals(2, assignment.getDomains().size());
        assertEquals(3, assignment.getDomains().get(exampleVar).size());
        assertEquals(2, assignment.getDomains().get(exampleVar2).size());

        assignment.assignValue(exampleVar2, 2);

        assertEquals(2, assignment.getDomains().size());
        assertEquals(3, assignment.getDomains().get(exampleVar).size());
        assertEquals(1, assignment.getDomains().get(exampleVar2).size());

        Assignment<Integer> newAssignment = assignment.createAssignmentWithNewValue(exampleVar, 1);

        assertEquals(2, assignment.getDomains().size());
        assertEquals(3, assignment.getDomains().get(exampleVar).size());
        assertEquals(1, assignment.getDomains().get(exampleVar2).size());

        assertEquals(1, newAssignment.getDomains().size());
        assertEquals(1, newAssignment.getDomains().get(exampleVar).size());
        try {
            assertEquals(1, newAssignment.getDomains().get(exampleVar2).size());
            fail();
        } catch (NullPointerException ignored) {

        }
    }

    @Test
    public void testGetDomainForVar() {
        assertEquals(3, assignment.getDomainForVar(exampleVar).size());
        assertEquals(2, assignment.getDomainForVar(exampleVar2).size());

        assignment.assignValue(exampleVar2, 2);

        assertEquals(3, assignment.getDomainForVar(exampleVar).size());
        assertEquals(1, assignment.getDomainForVar(exampleVar2).size());

        Assignment<Integer> newAssignment = assignment.createAssignmentWithNewValue(exampleVar, 1);

        assertEquals(3, assignment.getDomainForVar(exampleVar).size());
        assertEquals(1, assignment.getDomainForVar(exampleVar2).size());

        assertEquals(1, newAssignment.getDomainForVar(exampleVar).size());
        assertEquals(1, newAssignment.getDomainForVar(exampleVar2).size());
    }

    @Test
    public void testSetDomains() {
        HashMap<Variable<Integer>, List<Integer>> domains = new HashMap<>();
        assignment.setDomains(domains);

        assertEquals(domains, assignment.getDomains());
    }

    @Test
    public void testHasLocalEmptyDomains() {
        assertFalse(assignment.hasLocalEmptyDomains());

        Assignment<Integer> newAssignment = assignment.createAssignmentWithNewValue(exampleVar, 1);

        assertFalse(assignment.hasLocalEmptyDomains());
        assertFalse(newAssignment.hasLocalEmptyDomains());

        newAssignment.restrictDomainTo(exampleVar2, new ArrayList<>());

        assertFalse(assignment.hasLocalEmptyDomains());
        assertTrue(newAssignment.hasLocalEmptyDomains());
    }

    @Test
    public void testRestrictDomainTo() {
        assignment.restrictDomainTo(exampleVar, Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));

        assertEquals(9, assignment.getDomainForVar(exampleVar).size());
        assertEquals(2, assignment.getDomainForVar(exampleVar2).size());

        assignment.restrictDomainTo(exampleVar2, Arrays.asList(1, 2, 3, 4));

        assertEquals(9, assignment.getDomainForVar(exampleVar).size());
        assertEquals(4, assignment.getDomainForVar(exampleVar2).size());
    }

}
