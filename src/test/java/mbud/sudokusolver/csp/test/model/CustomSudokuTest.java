package mbud.sudokusolver.csp.test.model;

import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.constraint.Constraint;
import mbud.sudokusolver.csp.model.SudokuCSPCreator;
import mbud.sudokusolver.csp.model.impl.BasicSudokuCSPCreator;
import mbud.sudokusolver.util.Pair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

@RunWith(Parameterized.class)
public class CustomSudokuTest {

    private SudokuCSPCreator sudokuCSPCreator;
    private int width, height, expectedVariables, expectedConstraints;

    public CustomSudokuTest(int width, int height) {
        this.width = width;
        this.height = height;

        expectedVariables = (width * height * width * height);
        expectedConstraints = 3 * (width * height);

        sudokuCSPCreator = new BasicSudokuCSPCreator(width, height);
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {2, 2}, {3, 3}, {4, 4}, {5, 5}, {2, 4}, {3, 5}, {3, 2}, {4, 3}, {6, 5}, {7, 3}
        });
    }

    @Test
    public void test() {
        List<Variable<Integer>> variables = new ArrayList<>();
        List<Constraint<Integer>> constraints = new ArrayList<>();
        Map<Variable<Integer>, List<Constraint<Integer>>> constraintsForVars = new HashMap<>();

        sudokuCSPCreator.setupBoard(variables, constraints, constraintsForVars);
        assertEquals(variables.size(), expectedVariables);
        assertEquals(constraints.size(), expectedConstraints);

        int x = 1, y = 1;
        Pair<Integer, Integer> coord;
        Variable<Integer> v;
        for (int i = 0; i < variables.size(); i++) {
            x = 1 + (i / (width * height));
            y = 1 + (i % (width * height));
            v = variables.get(i);
            assertSame("For i=" + i, v, sudokuCSPCreator.valueAt(variables, x, y));
            coord = sudokuCSPCreator.coordOf(variables, v);
            assertEquals("For i=" + i, (long) x, (long) coord.getA());
            assertEquals("For i=" + i, (long) y, (long) coord.getB());
        }
    }

}
