package mbud.sudokusolver.csp.test.model;

import mbud.sudokusolver.csp.Variable;
import mbud.sudokusolver.csp.constraint.Constraint;
import mbud.sudokusolver.csp.model.SudokuCSPCreator;
import mbud.sudokusolver.csp.model.impl.BasicSudokuCSPCreator;
import mbud.sudokusolver.csp.model.impl.KillerSudokuCSPCreator;
import mbud.sudokusolver.csp.model.impl.SamuraiSudokuCSPCreator;
import mbud.sudokusolver.util.Pair;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class SudokuCSPCreatorTests {

    private SudokuCSPCreator samuraiModel, killerModel, sudokuCSPCreator;

    @Before
    public void setup() {
        samuraiModel = new SamuraiSudokuCSPCreator();
        killerModel = new KillerSudokuCSPCreator();
        ((KillerSudokuCSPCreator) killerModel).addGroup(10, 1, 1, 1, 2, 1, 3);
        ((KillerSudokuCSPCreator) killerModel).addGroup(4, 2, 1, 2, 2);
        ((KillerSudokuCSPCreator) killerModel).addGroup(23, 3, 1, 3, 2, 3, 3, 3, 4);
        ((KillerSudokuCSPCreator) killerModel).addGroup(11, 4, 1, 4, 2, 4, 3);
        ((KillerSudokuCSPCreator) killerModel).addGroup(16, 5, 1, 5, 2, 5, 3);
        sudokuCSPCreator = new BasicSudokuCSPCreator();
    }

    @Test
    public void basicModelTest() {
        List<Variable<Integer>> variables = new ArrayList<>();
        List<Constraint<Integer>> constraints = new ArrayList<>();
        Map<Variable<Integer>, List<Constraint<Integer>>> constraintsForVars = new HashMap<>();

        sudokuCSPCreator.setupBoard(variables, constraints, constraintsForVars);
        assertEquals(81, variables.size());
        assertEquals(27, constraints.size());

        int x = 1, y = 1;
        Pair<Integer, Integer> coord;
        Variable<Integer> v;
        for (int i = 0; i < variables.size(); i++) {
            x = 1 + (i / 9);
            y = 1 + (i % 9);
            v = variables.get(i);
            assertSame("For i=" + i, v, sudokuCSPCreator.valueAt(variables, x, y));
            coord = sudokuCSPCreator.coordOf(variables, v);
            assertEquals("For i=" + i, (long) x, (long) coord.getA());
            assertEquals("For i=" + i, (long) y, (long) coord.getB());
        }
    }

    @Test
    public void killerModelTest() {
        List<Variable<Integer>> variables = new ArrayList<>();
        List<Constraint<Integer>> constraints = new ArrayList<>();
        Map<Variable<Integer>, List<Constraint<Integer>>> constraintsForVars = new HashMap<>();

        killerModel.setupBoard(variables, constraints, constraintsForVars);
        assertEquals(81, variables.size());
        assertEquals(32, constraints.size());

        int x = 1, y = 1;
        Pair<Integer, Integer> coord;
        Variable<Integer> v;
        for (int i = 0; i < variables.size(); i++) {
            x = 1 + (i / 9);
            y = 1 + (i % 9);
            v = variables.get(i);
            assertSame("For i=" + i, v, killerModel.valueAt(variables, x, y));
            coord = killerModel.coordOf(variables, v);
            assertEquals("For i=" + i, (long) x, (long) coord.getA());
            assertEquals("For i=" + i, (long) y, (long) coord.getB());
        }

        assertEquals(4, constraintsForVars.get(killerModel.valueAt(variables, 1, 2)).size());
        assertEquals(3, constraintsForVars.get(killerModel.valueAt(variables, 2, 3)).size());
        assertEquals(4, constraintsForVars.get(killerModel.valueAt(variables, 3, 3)).size());
        assertEquals(3, constraintsForVars.get(killerModel.valueAt(variables, 7, 2)).size());
    }

    @Test
    public void samuraiModelTest() {
        List<Variable<Integer>> variables = new ArrayList<>();
        List<Constraint<Integer>> constraints = new ArrayList<>();
        Map<Variable<Integer>, List<Constraint<Integer>>> constraintsForVars = new HashMap<>();

        samuraiModel.setupBoard(variables, constraints, constraintsForVars);
        assertEquals(369, variables.size());
        assertEquals(131, constraints.size()); //131 = 41 (inner squares) + 45 (rows) + 45 (cols)

        int x = 1, y = 1;
        Pair<Integer, Integer> coord;
        Variable<Integer> v;
        for (int i = 0; i < 369; i++) {
            if (i < 81) { // Upper-left 9x9 square
                x = (i / 9) + 1;
                y = (i % 9) + 1;
            } else if (i < 162) { // Upper-right 9x9 square
                x = ((i - 81) / 9) + 1;
                y = ((i - 81) % 9) + 13;
            } else if (i < 243) { // Lower-left 9x9 square
                x = ((i - 162) / 9) + 13;
                y = ((i - 162) % 9) + 1;
            } else if (i < 324) { // Lower-right 9x9 square
                x = ((i - 243) / 9) + 13;
                y = ((i - 243) % 9) + 13;
            } else if (i < 333) { // Upper-middle 3x3 square
                x = ((i - 324) / 3) + 7;
                y = ((i - 324) % 3) + 10;
            } else if (i < 342) { // Lower-middle 3x3 square
                x = ((i - 333) / 3) + 13;
                y = ((i - 333) % 3) + 10;
            } else { // Middle 3x9 row
                x = ((i - 342) / 9) + 10;
                y = ((i - 342) % 9) + 7;
            }
            v = variables.get(i);
            assertSame("For i=" + i, v, samuraiModel.valueAt(variables, x, y));
            coord = samuraiModel.coordOf(variables, v);
            assertEquals("For i=" + i, (long) x, (long) coord.getA());
            assertEquals("For i=" + i, (long) y, (long) coord.getB());
        }
    }

}
