package mbud.sudokusolver.csp.test.algorithm;

import mbud.sudokusolver.csp.Assignment;
import mbud.sudokusolver.csp.SudokuCSP;
import mbud.sudokusolver.csp.algorithm.impl.Backtrack;
import mbud.sudokusolver.csp.model.impl.BasicSudokuCSPCreator;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class BasicSolveTests {

    @Test
    public void sudoku1Test() {
        SudokuCSP csp = new SudokuCSP(new BasicSudokuCSPCreator(3, 3));
        csp.assignStartingValue(csp.valueAt(1, 1), 6);
        csp.assignStartingValue(csp.valueAt(1, 3), 4);
        csp.assignStartingValue(csp.valueAt(1, 7), 3);
        csp.assignStartingValue(csp.valueAt(2, 6), 5);
        csp.assignStartingValue(csp.valueAt(2, 9), 1);
        csp.assignStartingValue(csp.valueAt(3, 1), 3);
        csp.assignStartingValue(csp.valueAt(3, 2), 8);
        csp.assignStartingValue(csp.valueAt(3, 7), 9);
        csp.assignStartingValue(csp.valueAt(4, 2), 2);
        csp.assignStartingValue(csp.valueAt(4, 4), 7);
        csp.assignStartingValue(csp.valueAt(4, 6), 1);
        csp.assignStartingValue(csp.valueAt(4, 7), 5);
        csp.assignStartingValue(csp.valueAt(5, 4), 9);
        csp.assignStartingValue(csp.valueAt(5, 6), 6);
        csp.assignStartingValue(csp.valueAt(5, 9), 4);
        csp.assignStartingValue(csp.valueAt(6, 1), 5);
        csp.assignStartingValue(csp.valueAt(6, 4), 4);
        csp.assignStartingValue(csp.valueAt(6, 6), 8);
        csp.assignStartingValue(csp.valueAt(7, 1), 1);
        csp.assignStartingValue(csp.valueAt(7, 4), 8);
        csp.assignStartingValue(csp.valueAt(8, 2), 9);
        csp.assignStartingValue(csp.valueAt(8, 7), 4);
        csp.assignStartingValue(csp.valueAt(8, 9), 6);
        csp.assignStartingValue(csp.valueAt(9, 2), 4);
        csp.assignStartingValue(csp.valueAt(9, 8), 7);
        csp.assignStartingValue(csp.valueAt(9, 9), 3);
        Backtrack<Integer> backtrack = new Backtrack<>(csp, false, false);
        Assignment<Integer> a = backtrack.solve();
        assertNotNull(a);
        if (a != null) {
            assertTrue(a.getDomainForVar(csp.valueAt(1, 4)).size() == 1);
            assertTrue(a.getDomainForVar(csp.valueAt(1, 4)).get(0) == 1);
        }
    }
}
